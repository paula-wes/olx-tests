package com.selenium.java.helper;

import com.selenium.java.base.BaseTest;
import com.selenium.java.listeners.TestListener;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import java.io.File;
import java.io.IOException;


public class WebHelper extends BaseTest {

    public void getScreenShot(String text) {
        WebDriver driver = getDriver();
        File file = new File("imagesFromTests/");
        String imagePath = file.getAbsolutePath();
        File srcFile;
        File targetFile;
        String pathToNewFile = imagePath + "/" + text;
        srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        targetFile = new File(pathToNewFile);
        try {
            FileUtils.copyFile(srcFile, targetFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void waitTime(int x) {
        try {
            Thread.sleep(x * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void tapCoordinates(double point_x, double point_y) {
        WebDriver driver = getDriver();
        new TouchAction((PerformsTouchActions) driver).tap(PointOption.point((int) point_x, (int) point_y)).perform();
    }

    public void testScreenShoot(String classname) {
        WebDriver driver = getDriver();
        TestListener testListener = new TestListener();
        if (driver instanceof  WebDriver) {
            testListener.saveScreenshotPNG(driver);
        }
        testListener.saveTextLog("Test: "+ classname + " failed and screenshot taken!");
    }
}



