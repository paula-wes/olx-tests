package com.selenium.java.pages.web;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.WebHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.awt.*;
import java.net.MalformedURLException;
import java.sql.Driver;


public class AdvertisementPage extends BaseTest {

    //region BUTTONS
    @FindBy(how = How.XPATH, using = "//*[@id=\"postNewAdLink\"]/span")
    public WebElement btn_addAdvert;

    @FindBy(how = How.ID, using = "add-title")
    public WebElement input_setTitle;

    @FindBy(how = How.XPATH, using = "//*[@id=\"targetrenderSelect1-0\"]/dt/a")
    public WebElement btn_chooseCategory;

    @FindBy(how = How.XPATH, using = "//*[@id=\"parameter-div-price\"]/div[3]/div/div[1]/div/div[1]/input")
    public WebElement input_price;

    @FindBy(how = How.XPATH, using = "//*[@id=\"parameter-div-price\"]/div[3]/div/div[1]/label[2]")
    public WebElement btn_chooseFree;

    @FindBy(how = How.XPATH, using = "//*[@id=\"parameter-div-price\"]/div[3]/div/div[1]/label[3]")
    public WebElement btn_chooseChange;

    @FindBy(how = How.XPATH, using = "//*[@id=\"targetparam13\"]/li[2]/a/span[1]")
    public WebElement btn_used;

    @FindBy(how = How.XPATH, using = "//*[@id=\"targetparam13\"]/li[3]/a/span[1]")
    public WebElement btn_new;

    @FindBy(how = How.XPATH, using = "//*[@id=\"targetid_private_business\"]/li[2]/a/span[1]")
    public WebElement btn_private;

    @FindBy(how = How.XPATH, using = "//*[@id=\"targetid_private_business\"]/li[3]/a/span[1]")
    public WebElement btn_company;

    @FindBy(how = How.ID, using = "add-description")
    public WebElement input_description;

    @FindBy(how = How.ID, using = "add-file-1")
    public WebElement btn_addPhoto1;

    @FindBy(how = How.ID, using = "mapAddress")
    public WebElement input_location;

    @FindBy(how = How.ID, using = "autosuggest-geo-ul")
    public WebElement chbx_suggestedCity;

    @FindBy(how = How.XPATH, using = "//*[@id=\"preview-link\"]/span")
    public WebElement btn_checkBeforeAdding;

    @FindBy(how = How.XPATH, using = "//*[@id=\"save-from-preview\"]/span")
    public WebElement btn_addReadyAdvert;

    @FindBy(how = How.XPATH, using = "//*[@id=\"innerLayout\"]/section/div[1]/ul/li[1]/div/div[3]/a[2]")
    public WebElement btn_addWithoutPromotion;

    //endregion BUTTONS


    //region METHODS

    WebHelper helper = new WebHelper();

    public void addAdvert() {

        System.out.println("Zalogowano się");

        btn_addAdvert.click();
        helper.waitTime(3);

        System.out.println("Wybrano opcję dodawania ogłoszenia");
    }

    public void setTitle(String title) {

        input_setTitle.sendKeys(title);
        System.out.println("Ustawiono tytuł ogłoszenia");
        helper.waitTime(5);
    }


    public void chooseCategory(String targetCat, String firstCat) {

        btn_chooseCategory.click();

        WebElement first_category = getDriver().findElement(By.xpath("//strong[@class='category-name block " +
                "lheight14 fnormal small' and contains(text(), '" + targetCat + "')]"));
        //strong to znacznik html, classa bierzesz tam gdzie masz nazwe kategorii i to category to pobieram z data provider
        first_category.click();

        helper.waitTime(5);

        WebElement target_category = getDriver().findElement(By.xpath("//strong[@class='category-name block " +
                "lheight14 fnormal small' and contains(text(), '" + firstCat + "')]"));
        target_category.click();


        System.out.println("Wybrano kategorię ogłoszenia");
        helper.waitTime(4);
    }

    public void setSalesMethod(String saleMethod, String price) {

        switch (saleMethod) {
            case "sell":
                input_price.sendKeys(price);
                break;
            case "give":
                btn_chooseFree.click();
                break;
            case "change":
                btn_chooseChange.click();
                break;
        }
        System.out.println("Wybrano metodę wydania przedmiotu");
        helper.waitTime(5);
    }


    public void setUsedOrNew(boolean used) {
        if (used) {
            btn_used.click();
        } else {
            btn_new.click();
        }

        System.out.println("Wybrano stan przedmiotu - używany lub nowy");
        helper.waitTime(3);
    }

    public void setPrivacy(boolean priv) {

        if (priv) {
            btn_private.click();
        } else {
            btn_company.click();
        }
        System.out.println("Wybrano prywatność - przedmiot sprzedany przez sprzedawcę prywatnego lub przez firmę");
        helper.waitTime(3);
    }

    public void setDescription(String description) {

        input_description.sendKeys(description);
        System.out.println("Wprowadzono opis przedmiotu");
        helper.waitTime(3);
    }

    public void addPhotos(String path) throws AWTException, MalformedURLException {

    //   btn_addPhoto1.click();
        
        WebElement addFile = getDriver().findElement(By.xpath(".//input[@type='file']"));
        addFile.sendKeys(path);
        helper.waitTime(10);

        System.out.println("Wybrano i dodano zdjęcia");

    }

    public void setContactData(String location) {

        input_location.clear(); //czyszczenie pola tekstowego
        helper.waitTime(2);
        input_location.sendKeys(location);
        helper.waitTime(2);
        chbx_suggestedCity.click();

        System.out.println("Dodano lokalizację");
        helper.waitTime(3);
    }

    public void checkBeforeAdding() {

        btn_checkBeforeAdding.click();
        System.out.println("Sprawdzono ogłoszenie przed dodaniem");
        helper.waitTime(3);
    }

    public void addReadyAdvert() {
        btn_addReadyAdvert.click();
        helper.waitTime(3);
        btn_addWithoutPromotion.click();
        System.out.println("Dodano ogłoszenie");
        helper.waitTime(3);

    }


//endregion METHODS

}
