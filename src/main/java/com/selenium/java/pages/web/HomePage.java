package com.selenium.java.pages.web;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.WebHelper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;



public class HomePage extends BaseTest {

    //region BUTTONS
    @FindBy(how = How.ID, using = "my-account-link")
    public WebElement input_myAccount;

    @FindBy(how = How.ID, using = "userEmail")
    public WebElement input_eMail;

    @FindBy(how = How.ID, using = "userPass")
    public WebElement input_password;

    @FindBy(how = How.CLASS_NAME, using = "cookiesBarClose")
    public WebElement btn_closeCookie;

    @FindBy(how = How.ID, using = "se_userLogin")
    public WebElement btn_login;

    @FindBy(how = How.ID, using = "register_tab")
    public WebElement btn_register;

    @FindBy(how = How.ID, using = "userEmailRegister")
    public WebElement input_emailRegister;

    @FindBy(how = How.ID, using = "userPassRegister")
    public WebElement input_passRegister;

    @FindBy(how = How.XPATH, using = "//*[@id=\"registerForm\"]/div[4]/div/div/label[2]")
    public WebElement chbx_acceptance;

    @FindBy(how = How.ID, using = "button_register")
    public WebElement btn_registerDone;

    //endregion BUTTONS


    //region METHODS COOKIES
    public void closeCookies(){

        btn_closeCookie.click();

        System.out.println("Cookies zostały zamknięte");
    }
    //endregion COOKIES


    //region METHODS LOGIN TO APP

    WebHelper helper = new WebHelper();

    public void loginToApp(String email, String pass){

        input_myAccount.click();
        input_eMail.sendKeys(email);
        System.out.println("Wpisano adres email: " + email);
        helper.waitTime(3);
        input_password.sendKeys(pass);
        System.out.println("Wpisano hasło: " + pass);

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        btn_login.click();
        helper.waitTime(3);
        }

    //endregion METHODS LOGIN TO APP


    //region METHODS REGISTER TO APP

    public void registerToApp(String email, String pass){

        input_myAccount.click();
        helper.waitTime(2);
        btn_register.click();
        System.out.println("Wybrano opcję rejestracji konta");
        helper.waitTime(3);
        input_emailRegister.click();
        input_emailRegister.sendKeys(email);
        System.out.println("Wpisano adres email: " + email);
        helper.waitTime(2);
        input_passRegister.click();
        input_passRegister.sendKeys(pass);
        System.out.println("Wpisano hasło: " + pass);
        helper.waitTime(4);
        chbx_acceptance.click();
        System.out.println("Zaakceptowano warunki rejestracji konta");
        helper.waitTime(5);
        btn_registerDone.click();
        System.out.println("Dokonano rejestracji konta");

    }


    //endregion METHODS REGISTER TO APP
}
