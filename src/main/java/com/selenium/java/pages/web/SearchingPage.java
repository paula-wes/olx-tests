package com.selenium.java.pages.web;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.WebHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;


public class SearchingPage extends BaseTest {

    //region BUTTONS
    @FindBy(how = How.ID, using = "headerSearch")
    public WebElement input_searchItem;

    @FindBy(how = How.XPATH, using = "//*[@id=\"paramsListOpt\"]/div/div[1]/label[1]")
    public WebElement chbx_searchInDesc;

    @FindBy(how = How.XPATH, using = "//*[@id=\"paramsListOpt\"]/div/div[2]/label[1]")
    public WebElement chbx_searchWithPhoto;

    @FindBy(how = How.ID, using = "cityField")
    public WebElement input_searchLocation;

    @FindBy(how = How.XPATH, using = "//*[@id=\"param_price\"]/div[2]/div[1]/a/span[1]")
    public WebElement btn_priceFrom;

    @FindBy(how = How.XPATH, using = "//*[@id=\"param_price\"]/div[2]/div[2]/a/span[1]")
    public WebElement btn_priceTo;

    @FindBy(how = How.XPATH, using = "//*[@id=\"param_price\"]/div[2]/div[1]/label/input")
    public WebElement input_priceFrom;

    @FindBy(how = How.XPATH, using = "//*[@id=\"param_price\"]/div[2]/div[2]/label/input")
    public WebElement input_priceTo;

    @FindBy(how = How.ID, using = "targetorder-select-gallery")
    public WebElement chbx_sort;

    @FindBy(how = How.XPATH, using = "//*[@id=\"targetorder-select-gallery\"]/dd/ul/li[1]/a")
    public WebElement chbx_latest;

    @FindBy(how = How.XPATH, using = "//*[@id=\"targetorder-select-gallery\"]/dd/ul/li[2]/a")
    public WebElement chbx_cheapest;

    @FindBy(how = How.XPATH, using = "//*[@id=\"targetorder-select-gallery\"]/dd/ul/li[3]/a")
    public WebElement chbx_expensive;

    @FindBy(how = How.XPATH, using = "//*[@id=\"viewSelector\"]/li[3]/a")
    public WebElement btn_galleryView;

    @FindBy(how = How.XPATH, using = "//*[@id=\"viewSelector\"]/li[2]/span")
    public WebElement btn_iconView;

    @FindBy(how = How.XPATH, using = "//*[@id=\"tabs-container\"]/div/div[2]/ul/li[1]")
    public WebElement btn_all;

    @FindBy(how = How.XPATH, using = "//*[@id=\"tabs-container\"]/div/div[2]/ul/li[2]/a")
    public WebElement btn_private;

    @FindBy(how = How.XPATH, using = "//*[@id=\"tabs-container\"]/div/div[2]/ul/li[3]/a")
    public WebElement btn_company;

    @FindBy(how = How.XPATH, using = "//*[@id=\"saveSearchCriteriaTop\"]")
    public WebElement btn_followSearches;

    @FindBy(how = How.XPATH, using = "//*[@id=\"param_state\"]/div[2]/a/span[1]")
    public WebElement chbx_condition;

    @FindBy(how = How.XPATH, using = "//*[@id=\"param_state\"]/div[2]/ul/li[2]/label[2]")
    public WebElement chbx_used;

    @FindBy(how = How.XPATH, using = "//*[@id=\"param_state\"]/div[2]/ul/li[3]/label[2]")
    public WebElement chbx_new;

    //endregion BUTTONS

    //region METHODS

    WebHelper helper = new WebHelper();

    public void searchItem(String item, String category1) {

        input_searchItem.sendKeys(item);
        WebElement btn_category = getDriver().findElement(By.xpath("//span[@class='fbold' and contains(text(), '" + category1 + "')]"));
        helper.waitTime(3);
        btn_category.click();
        System.out.println("Wpisano nazwę przedmiotu w pole wyszukiwania");

        helper.waitTime(3);
    }

    public void searchInDesc(boolean searchInDesc) {

        if (searchInDesc) {
            chbx_searchInDesc.click();
        } else {

        }
        System.out.println("Dokonano wyboru czy nazwa przedmiotu ma być wyszukiwana również w opisach");
        helper.waitTime(3);
    }

    public void searchWithPhoto(boolean photo) {

        if (photo) {
            chbx_searchWithPhoto.click();
        } else {

        }
        helper.waitTime(3);
        System.out.println("Dokonano wyboru czy mają być wyszukiwane przedmioty tylko ze zdjęciem");
    }

    public void location(String city, String voivodeship) {

        input_searchLocation.click();
        input_searchLocation.sendKeys(city);
        helper.waitTime(2);

        WebElement choose_voivodeship = getDriver().findElement(By.xpath("//span[@class='color-9' and contains(text(), '" + voivodeship + "')]"));
        helper.waitTime(2);
        choose_voivodeship.click();
        helper.waitTime(3);

        System.out.println("Dodano lokalizację");
    }


    public void setPrice(String priceFrom, String priceTo) {

        btn_priceFrom.click();
        helper.waitTime(5);
        input_priceFrom.sendKeys(priceFrom);
        System.out.println("Wprowadzono cenę minimalną");
        helper.waitTime(5);
        btn_priceTo.click();
        helper.waitTime(5);
        input_priceTo.sendKeys(priceTo);
        System.out.println("Wprowadzono cenę maksymalną");
    }

    public void setCondition(boolean condition) {

        helper.waitTime(2);
        chbx_condition.click();
        if (condition) {
            chbx_used.click();
            System.out.println("Wybrano stan przedmiotów - używany");
        } else {
            chbx_new.click();
            System.out.println("Wybrano stan przedmiotów - nowy");
        }
        helper.waitTime(5);
    }


    public void sortBy(String sortBy) {


        // I metoda sortowania
//        chbx_sort.click();
//        helper.waitTime(3);
//        WebElement chbx_sortBy = getDriver().findElement(By.xpath("//a[@contains(text(), '" + sortBy + "')]"));
//        helper.waitTime(4);
//        chbx_sortBy.click();
//        helper.waitTime(3);
//        System.out.println("Wybrano sposób sortowania");


        // II metoda sortowania
        chbx_sort.click();
        helper.waitTime(3);
        switch (sortBy) {
            case "latest":
                chbx_latest.click();
                System.out.println("Wybrano sortowanie produktów od najnowszych");
                break;
            case "cheapest":
                chbx_cheapest.click();
                System.out.println("Wybrano sortowanie produktów od najtańszych");
                break;
            case "expensive":
                chbx_expensive.click();
                System.out.println("Wybrano sortowanie produktów od najdroższych");
                break;
        }
        helper.waitTime(4);


    }

    public void viewSelector(boolean view) {

        if (view) {
            btn_galleryView.click();
        } else {
            btn_iconView.click();
        }
        System.out.println("Wybrano sposób prezentacji ogłoszeń - lista lub galeria");
        helper.waitTime(3);
    }

    public void setPrivacy(String priv) {

        helper.waitTime(2);
        switch (priv) {
            case "all":
                btn_all.click();
                break;
            case "private":
                btn_private.click();
                break;
            case "company":
                btn_company.click();
                break;
        }
        System.out.println("Wybrano sprzedawcę - prywatna osoba lub firma");
        helper.waitTime(3);
    }

    public void followSearches() {

        btn_followSearches.click();
        System.out.println("Dodano wyszukiwania do obserwowanych");
        helper.waitTime(3);

    }


    //endregion METHODS

}
