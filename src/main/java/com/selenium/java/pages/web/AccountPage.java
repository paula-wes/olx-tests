package com.selenium.java.pages.web;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.WebHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;


public class AccountPage extends BaseTest {

    //region BUTTONS
    @FindBy(how = How.ID, using = "my-account-link")
    public WebElement btn_myAccount;

    @FindBy(how = How.ID, using = "se_accountShop")
    public WebElement btn_settings;

    @FindBy(how = How.ID, using = "se_changeDefault")
    public WebElement btn_changeData;

    @FindBy(how = How.ID, using = "geoCity")
    public WebElement input_geoCity;

    @FindBy(how = How.ID, using = "defaultPerson")
    public WebElement input_personName;

    @FindBy(how = How.ID, using = "submitDefault")
    public WebElement btn_submit;

    @FindBy(how = How.ID, using = "se_changePassword")
    public WebElement btn_changePassword;

    @FindBy(how = How.ID, using = "se_chPassword")
    public WebElement input_oldPass;

    @FindBy(how = How.ID, using = "se_chRepeatPassword")
    public WebElement input_newPass;

    @FindBy(how = How.ID, using = "passwordInput")
    public WebElement btn_passwordInput;

    @FindBy(how = How.ID, using = "se_changeEmail")
    public WebElement btn_changeEmail;

    @FindBy(how = How.ID, using = "changeEmailEmail")
    public WebElement input_newEmail;

    @FindBy(how = How.ID, using = "mailSubmit")
    public WebElement btn_mailSubmit;

    @FindBy(how = How.ID, using = "se_cv")
    public WebElement btn_addCV;

    @FindBy(how = How.XPATH, using = "//*[@id=\"body-container\"]/div/div[1]/div[5]/div[1]/div/span[2]")
    public WebElement btn_attachCV;

    @FindBy(how = How.ID, using = "se_invoiceForm")
    public WebElement btn_invoiceForm;

    @FindBy(how = How.ID, using = "name")
    public WebElement input_companyName;

    @FindBy(how = How.ID, using = "address")
    public WebElement input_street;

    @FindBy(how = How.ID, using = "postcode")
    public WebElement input_postcode;

    @FindBy(how = How.ID, using = "city")
    public WebElement input_city;

    @FindBy(how = How.ID, using = "nip")
    public WebElement input_nip;

    @FindBy(how = How.ID, using = "saveInvoiceData")
    public WebElement btn_saveInvoiceData;

    @FindBy(how = How.ID, using = "login-box-logout")
    public WebElement btn_logOut;

    //endregion BUTTONS

    //region METHODS

    WebHelper helper = new WebHelper();

    public void goToMyAccount() {
        btn_myAccount.click();
        System.out.println("Otworzono zakładkę Mój OLX ");
        helper.waitTime(2);
    }

    public void goToSettings(){
        btn_settings.click();
        System.out.println("Otworzono zakładkę Ustawienia ");
        helper.waitTime(2);
    }

    public void changeContactData(String city, String voivodeship){
        btn_changeData.click();
        System.out.println("Wybrano opcję zmiany danych kontaktowych");
        helper.waitTime(2);
        input_geoCity.clear();
        input_geoCity.sendKeys(city);
        helper.waitTime(2);
        WebElement choose_place = getDriver().findElement(By.xpath("//span[@class='color-9' and contains(text(), '" + voivodeship + "')]"));
        helper.waitTime(2);
        choose_place.click();
        System.out.println("Zmieniono lokalizację");
        input_personName.clear();
        helper.waitTime(2);
        input_personName.sendKeys("Paula");
        helper.waitTime(2);
        System.out.println("Zmieniono nazwę osoby kontaktowej");
        btn_submit.click();
        helper.waitTime(2);
        System.out.println("Zapisano zmianę danych");
    }

    public void changePassword(String oldPass, String newPass){
        btn_changePassword.click();
        input_oldPass.sendKeys(oldPass);
        System.out.println("Wprowadzono stare hasło " + oldPass);
        helper.waitTime(2);
        input_newPass.sendKeys(newPass);
        System.out.println("Wprowadzono nowe hasło " + newPass);
        helper.waitTime(2);
        btn_passwordInput.click();
        System.out.println("Zapisano zmiany");
        helper.waitTime(2);
    }

    public void changeEmail(String mail){
        btn_changeEmail.click();
        helper.waitTime(3);
        input_newEmail.sendKeys(mail);
        helper.waitTime(3);
        System.out.println("Wprowadzono nowy adres e-mail");
        btn_mailSubmit.click();
        System.out.println("Zapisano zmiany");
    }


    public void attachCV(String path) {
        btn_addCV.click();
        System.out.println("Wybrano opcję dodawania CV");
     //   btn_attachCV.click();
        helper.waitTime(2);
        WebElement addFile = getDriver().findElement(By.xpath(".//input[@type='file']"));
        addFile.sendKeys(path);
        helper.waitTime(10);
        System.out.println("CV zostało dodane");
    }

    public void invoiceDetails(String name, String street, String postcode, String city, String nip){

        helper.waitTime(3);
        btn_invoiceForm.click();
        System.out.println("Wybrano opcję zmiany danych do faktury" );
        helper.waitTime(2);
        input_companyName.clear();
        input_companyName.sendKeys(name);
        System.out.println("Wprowadzono nazwę firmy: " + name);
        helper.waitTime(2);
        input_street.clear();
        input_street.sendKeys(street);
        System.out.println("Wprowadzono ulicę: " + street);
        helper.waitTime(2);
        input_postcode.clear();
        input_postcode.sendKeys(postcode);
        System.out.println("Wprowadzono kod pocztowy: " + postcode);
        helper.waitTime(2);
        input_city.clear();
        input_city.sendKeys(city);
        System.out.println("Wprowadzono miasto: " + city);
        helper.waitTime(2);
        input_nip.clear();
        input_nip.sendKeys(nip);
        System.out.println("Wprowadzono NIP: " + nip);
        helper.waitTime(2);
        btn_saveInvoiceData.click();
        System.out.println("Zapisano zmiany");
    }

    public void logOut(){
        WebDriver driver = getDriver();
        Actions action = new Actions(driver);
        WebElement element = driver.findElement(By.id("my-account-link"));
        action.moveToElement(element).build().perform();
        helper.waitTime(3);
        btn_logOut.click();
        System.out.println("Wylogowano z aplikacji");

    }
    //endregion METHODS

}
