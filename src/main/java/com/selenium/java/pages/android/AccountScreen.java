package com.selenium.java.pages.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.Helper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class AccountScreen extends BaseTest {

    //region PRZYCISKI

    @FindBy(how = How.XPATH, using = "//android.widget.FrameLayout[@content-desc=\"Konto\"]/android.widget.ImageView")
    public WebElement btn_account;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/" +
            "android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[1]/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[5]/android.widget.TextView")
    public WebElement btn_settings;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Edytuj profil')]")
    public WebElement btn_editProfile;

    @FindBy(how = How.ID, using = "pl.tablica:id/userNameInput")
    public WebElement input_changeName;

    @FindBy(how = How.ID, using = "pl.tablica:id/saveButton")
    public WebElement btn_save;

    @FindBy(how = How.XPATH, using = "//android.widget.ImageButton[@content-desc=\"Przejdź wyżej\"]")
    public WebElement btn_undo;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/" +
            "android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/" +
            "android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]")
    public WebElement btn_changePassword;

    @FindBy(how = How.ID, using = "pl.tablica:id/currentPasswordText")
    public WebElement input_currentPassword;

    @FindBy(how = How.ID, using = "pl.tablica:id/newPasswordText")
    public WebElement input_newPassword;

    @FindBy(how = How.ID, using = "pl.tablica:id/doneButton")
    public WebElement btn_done;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Zmień e-mail')]")
    public WebElement btn_changeEmail;

    @FindBy(how = How.ID, using = "pl.tablica:id/edtNewMail")
    public WebElement input_changeEmail;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/" +
            "android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/" +
            "android.view.ViewGroup/android.widget.FrameLayout/" +
            "android.widget.FrameLayout/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[4]/" +
            "android.widget.TextView")
    public WebElement btn_notifications;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/" +
            "android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/" +
            "android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.view.ViewGroup[1]/android.widget.Switch")
    public WebElement switch_tipsEmail;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/" +
            "android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/" +
            "android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/" +
            "android.widget.LinearLayout[2]/android.view.ViewGroup[1]/android.widget.Switch")
    public WebElement switch_messagesEmail;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/" +
            "android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/" +
            "android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/" +
            "android.widget.LinearLayout[2]/android.view.ViewGroup[1]/android.widget.Switch")
    public WebElement switch_newAddsApp;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/" +
            "android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/" +
            "android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/" +
            "android.widget.LinearLayout[4]/android.view.ViewGroup[1]/android.widget.Switch")
    public WebElement switch_discountEmail;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Dane do faktury')]")
    public WebElement btn_invoiceDetails;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Twoje CV')]")
    public WebElement btn_yourCV;

    @FindBy(how = How.ID, using = "pl.tablica:id/attachBtn")
    public WebElement btn_attachCV;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'template1-placeholder.jpg')]")
    public WebElement btn_chooseCVjpg;

    @FindBy(how = How.ID, using = "com.android.documentsui:id/option_menu_search")
    public WebElement btn_searchFor;

    @FindBy(how = How.ID, using = "com.android.documentsui:id/search_src_text")
    public WebElement input_searchByText;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'CV - P.W..pdf')]")
    public WebElement btn_chooseCVpdf;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/" +
            "android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.RelativeLayout/android.webkit.WebView/" +
            "android.webkit.WebView/android.view.View/android.view.View[3]/" +
            "android.view.View/android.view.View[2]/android.view.View[1]/android.widget.EditText[1]")
    public WebElement input_companyName;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/" +
            "android.widget.FrameLayout[2]/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[3]/" +
            "android.view.View/android.view.View[2]/android.view.View[1]/android.widget.EditText[2]")
    public WebElement input_streetName;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/" +
            "android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[3]/" +
            "android.view.View/android.view.View[2]/android.view.View[1]/android.widget.EditText[3]")
    public WebElement input_postCode;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/" +
            "android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[3]/" +
            "android.view.View/android.view.View[2]/android.view.View[1]/android.widget.EditText[4]")
    public WebElement input_city;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.RelativeLayout/" +
            "android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[3]/" +
            "android.view.View/android.view.View[2]/android.view.View[1]/android.widget.EditText[5]")
    public WebElement input_nip;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Zmień')]")
    public WebElement btn_change;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Tryb ciemny')]")
    public WebElement btn_mode;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Ciemny')]")
    public WebElement btn_darkMode;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Jasny')]")
    public WebElement btn_brightMode;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Automatyczny')]")
    public WebElement btn_automaticMode;


    //endregion PRZYCISKI


    //region METODY

    Helper helper = new Helper();
    public void setAccount(){

        btn_account.click();
        System.out.println("Weszliśmy w funkcje Konta");
        while (helper.swipeToElementByText("Wyloguj")) {
            helper.swipeInDirection(direction.UP, "up", 0.6);
        }


    }

    public void accountSettings() {
        btn_settings.click();
        System.out.println("Otworzono ustawienia konta");
    }


    public void editProfile(String name) {
        btn_editProfile.click();
        input_changeName.click();
        input_changeName.sendKeys(name);
        helper.pressAndroidBackBtn();

        btn_save.click();

        btn_undo.click();

        System.out.println("Zmieniono nazwę/ imię posiadacza konta");
//        try {
//            Thread.sleep(10000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

    }

    public void passwordSettingsShort(String oldPass, String newPass) {
        // hasło zbyt krótkie

        btn_changePassword.click();
        input_currentPassword.click();
        input_currentPassword.sendKeys(oldPass);
        helper.pressAndroidBackBtn();

        input_newPassword.click();
        input_newPassword.sendKeys(newPass);
        helper.pressAndroidBackBtn();
        btn_done.click();


        System.out.println("Wprowadzono zbyt krótkie hasło");
    }

    public void passwordSettingsWrongOld(String oldPass, String newPass) {

        // prawidłowe nowe hasło, błędne stare hasło
        btn_changePassword.click();
        input_currentPassword.click();
        input_currentPassword.sendKeys(oldPass);
        helper.pressAndroidBackBtn();
        input_newPassword.click();
        input_newPassword.sendKeys(newPass);
        helper.pressAndroidBackBtn();
        btn_done.click();



        System.out.println("Wprowadzono błędne hasło");
    }



    public void emailSettWrongEmail(String wrongEmail) {
        //błędny email
        btn_changeEmail.click();
        input_changeEmail.click();
        input_changeEmail.sendKeys(wrongEmail);
        helper.pressAndroidBackBtn();
        btn_done.click();



        System.out.println("Wprowadzono niepoprawny adres e-mail");
    }

    public void emailSettUsedEmail(String usedEmail) {
        // email już wykorzystany
        btn_changeEmail.click();
        input_changeEmail.click();
        input_changeEmail.sendKeys(usedEmail);
        helper.pressAndroidBackBtn();
        btn_done.click();



        System.out.println("Wprowadzono adres e-mail, który jest już wykorzystany");
    }

    public void notificationsSettings() {

        btn_notifications.click();
        switch_tipsEmail.click();
        switch_messagesEmail.click();

        while (helper.swipeToElementByText("Rabaty")) {
            helper.swipeInDirection(direction.UP, "up", 0.5);
        }

        switch_newAddsApp.click();
        switch_discountEmail.click();

        btn_undo.click();



        System.out.println("Zmieniono ustawienia powiadomień");

    }

    public void invoiceDetailsSettings(String company, String street, String postCode, String city, String nip) {
        btn_invoiceDetails.click();
        System.out.println("Zmiana detali niezbędnych do faktury");
        input_companyName.click();
        input_companyName.sendKeys(company);
        helper.pressAndroidBackBtn();
        System.out.println("Wprowadzono nazwę firmy");
        input_streetName.click();
        input_streetName.sendKeys(street);
        helper.pressAndroidBackBtn();
        System.out.println("Wprowadzono ulicę");
        input_postCode.click();
        input_postCode.sendKeys(postCode);
        helper.pressAndroidBackBtn();
        System.out.println("Wprowadzono kod pocztowy");
        input_city.click();
        input_city.sendKeys(city);
        helper.pressAndroidBackBtn();
        System.out.println("Wprowadzono miasto");
        input_nip.click();
        input_nip.sendKeys(nip);
        helper.pressAndroidBackBtn();
        System.out.println("Wprowadzono nip");
        btn_change.click();
        System.out.println("Wprowadzono zmiany w danych potrzebnych do faktury");

    }

    public void addCVSettings() {
        btn_yourCV.click();
        btn_attachCV.click();

//        // CV w jpg
//
//        while (helper.swipeToElementByText("CV_wzory_informatyk1.png")) {
//            helper.swipeInDirection(direction.UP, "up", 0.8);
//        }
//        btn_chooseCVjpg.click();
//        System.out.println("Podjęto próbę załączenia CV w formacie jpg");
//        // CV w pdf
//        btn_attachCV.click();
        btn_searchFor.click();
        input_searchByText.click();
        input_searchByText.sendKeys("CV");

        btn_chooseCVpdf.click();
        System.out.println("Załączono CV w formacie pdf");

        helper.getScreenShot("afterAttachedCV.png");



    }

    public void changeMode() {

        btn_mode.click();
        btn_brightMode.click();
        btn_darkMode.click();
        btn_automaticMode.click();
        System.out.println("Zmieniono tryb aplikacji");
    }



    //endregion METODY
}
