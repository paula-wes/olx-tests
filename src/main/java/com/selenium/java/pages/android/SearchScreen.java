package com.selenium.java.pages.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.Helper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SearchScreen extends BaseTest {

    @FindBy(how = How.ID, using = "pl.tablica:id/searchInput")
    public WebElement btn_search;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Znajdź coś dla siebie')]")
    public WebElement btn_search2;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'w Samochody osobowe')]")
    public WebElement input_search;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/" +
            "android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.ScrollView/androidx.recyclerview.widget." +
            "RecyclerView/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[1]\n")
    public WebElement btn_chooseCategory;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Filtruj')]")
    public WebElement btn_filter;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Najdroższe')]")
    public WebElement btn_sortBy;

    @FindBy(how = How.XPATH, using = "//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/" +
            "android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/" +
            "android.widget.LinearLayout/android.widget.LinearLayout[4]/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ImageButton")
    public WebElement btn_crossLocation;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Wybierz')]")
    public WebElement btn_chooseLocation;

    @FindBy(how = How.ID, using = "pl.tablica:id/searchInput")
    public WebElement input_setCity;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/" +
            "android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.view.ViewGroup[1]\n")
    public WebElement btn_selectCity;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/" +
            "android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ImageButton[2]")
    public WebElement btn_addDistance;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup" +
            "/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.view.ViewGroup/android.widget.LinearLayout[2]/" +
            "android.widget.FrameLayout/android.view.ViewGroup/android.widget.EditText[1]\n")
    public WebElement input_priceFrom;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/" +
            "android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.view.ViewGroup/android.widget.LinearLayout[2]/" +
            "android.widget.FrameLayout/android.view.ViewGroup/android.widget.EditText[2]")
    public WebElement input_priceTo;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/" +
            "android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.view.ViewGroup/android.widget.EditText[1]\n")
    public WebElement input_yearProdFrom;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/" +
            "android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.view.ViewGroup/android.widget.EditText[2]\n")
    public WebElement input_yearProdTo;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Benzyna')]")
    public WebElement btn_petrol;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Diesel')]")
    public WebElement btn_diesel;

    @FindBy(how = How.ID, using = "pl.tablica:id/chooserBtn")
    public WebElement chbx_chooseBodyType;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Sedan')]")
    public WebElement btn_sedan;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Coupe')]")
    public WebElement btn_coupe;

    @FindBy(how = How.ID, using = "pl.tablica:id/btnSubmit")
    public WebElement btn_ready;

    @FindBy(how = How.ID, using = "pl.tablica:id/see_more_params_button")
    public WebElement btn_moreParameters;

    @FindBy(how = How.XPATH, using ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/" +
            "android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/" +
            "android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.FrameLayout" +
            "/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText")
    public WebElement chbx_color;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Biały')]")
    public WebElement chbx_chooseWhite;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Czarny')]")
    public WebElement chbx_chooseBlack;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Srebrny')]")
    public WebElement chbx_chooseSilver;

    @FindBy(how = How.ID, using = "pl.tablica:id/btnSubmit")
    public WebElement btn_acceptColors;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Nieuszkodzony')]")
    public WebElement btn_technicalCondition;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Automatyczna')]")
    public WebElement btn_gearbox;

    @FindBy(how = How.ID, using = "pl.tablica:id/btnSubmit")
    public WebElement btn_showResults;

    @FindBy(how = How.ID, using = "pl.tablica:id/favoriteButton")
    public WebElement btn_addToFave;

    Helper helper = new Helper();

    public void searchItems(String item) {

        WebDriver driver = getDriver();

        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        btn_search.click();

        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        new Actions(driver).sendKeys(item).perform();
        btn_search.click();
        input_search.click();
    }

//    public void choosingCategory() {
//        btn_chooseCategory.click();
//    }


    public void filterItems(String city) {
        btn_filter.click();
        btn_sortBy.click();
        btn_crossLocation.click();
        btn_chooseLocation.click();
        input_setCity.click();
        input_setCity.sendKeys(city);
        btn_selectCity.click();
        btn_addDistance.click();
        btn_addDistance.click();
        btn_addDistance.click();

        while (helper.swipeToElementByText("Typ nadwozia")) {
            helper.swipeInDirection(direction.UP, "up", 0.5);
        }
    }

    public void setPrice(String priceFrom, String priceTo) {

        input_priceFrom.click();
        input_priceFrom.sendKeys(priceFrom);

        input_priceTo.click();
        input_priceTo.sendKeys(priceTo);
        helper.pressAndroidBackBtn();

    }


    public void setYearOfProd(String yearProdFrom, String yearProdTo) {
        input_yearProdFrom.click();
        input_yearProdFrom.sendKeys(yearProdFrom);
        helper.pressAndroidBackBtn();
        input_yearProdTo.click();
        input_yearProdTo.sendKeys(yearProdTo);
        helper.pressAndroidBackBtn();
    }

    public void setFuel() {
        btn_petrol.click();
        btn_diesel.click();
    }

    public void bodyType() {
        chbx_chooseBodyType.click();
        btn_sedan.click();
        btn_coupe.click();
        btn_ready.click();

    }

    public void moreFunction(){
        btn_moreParameters.click();
        while (helper.swipeToElementByText("Kierownica")) {
            helper.swipeInDirection(direction.UP, "up", 0.4);
        }
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        chbx_color.click();
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        chbx_chooseBlack.click();
        chbx_chooseWhite.click();
        chbx_chooseSilver.click();
        btn_acceptColors.click();

        btn_technicalCondition.click();
        btn_gearbox.click();

    }

    public void showResults() {
        btn_showResults.click();

    }

//    public void swipetext(String text_komponentu,BaseTest.direction kierunek, String kierunek_poczatkowy, double moc, int ile_razy) {
//        int i=0;
//        while (helper.swipeToElementByText("ZAMKNIJ") && i<3) {
//            helper.swipeInDirection(direction.UP, "UP", 0.8);
//            i++;
//        } }



}
