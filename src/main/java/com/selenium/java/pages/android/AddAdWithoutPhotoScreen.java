package com.selenium.java.pages.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.Helper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class AddAdWithoutPhotoScreen extends BaseTest {

    //region PRZYCISKI
    //region PRZYCISKI
    @FindBy(how = How.XPATH, using = "//android.widget.FrameLayout[@content-desc=\"Dodaj\"]/android.widget.ImageView")
    public WebElement btn_add;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/" +
            "android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText")
    public WebElement input_adTitle;

    @FindBy(how = How.ID, using = "pl.tablica:id/chooserBtn")
    public WebElement btn_chooseCategory;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Prywatne')]")
    public WebElement btn_private;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Firmowe')]")
    public WebElement btn_company;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Nowe')]")
    public WebElement btn_conditionNew;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Używane')]")
    public WebElement btn_conditionUsed;

    @FindBy(how = How.ID, using = "pl.tablica:id/priceInput")
    public WebElement input_enterThePrice;

    @FindBy(how = How.ID, using = "pl.tablica:id/priceArranged")
    public WebElement btn_priceIsNegiotiable;

    @FindBy(how = How.ID, using = "pl.tablica:id/inputText")
    public WebElement input_addDescription;

    @FindBy(how = How.ID, using = "pl.tablica:id/previewAdBtn")
    public WebElement btn_seeBeforeAdding;

    @FindBy(how = How.ID, using = "pl.tablica:id/submit")
    public WebElement btn_adAdd;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Dodaj bez promowania')]")
    public WebElement btn_adWithoutPromotion;

    @FindBy(how = How.ID, using = "pl.tablica:id/backToHP")
    public WebElement btn_backToHomePage;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/" +
            "android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/" +
            "android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/" +
            "android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/" +
            "android.widget.FrameLayout/android.widget.EditText")
    public WebElement input_addPhoneNumber;


    //endregion PRZYCISKI
    Helper helper = new Helper();

    public void addAdvert() {
        btn_add.click();
    }

    public void setTitle(String title) {

        input_adTitle.sendKeys(title);

        System.out.println("Dodano tytuł ogłoszenia");
    }

    public void chooseCategory(String catTarg, String cat1, String cat2) {

        btn_chooseCategory.click();

        WebElement target_cat = getDriver().findElement(By.xpath("//*[contains(@text, '" + catTarg + "')]"));
        WebElement first_cat = getDriver().findElement(By.xpath("//*[contains(@text, '" + cat1 + "')]"));
        WebElement second_cat = getDriver().findElement(By.xpath("//*[contains(@text, '" + cat2 + "')]"));

        try {

            target_cat.click();

        } catch (Exception e) {
            try {
                while (helper.swipeToElementByText("//*[contains(@text, '" + first_cat + "')]")) {
                    helper.swipeInDirection(direction.UP, "up", 0.8);
                    first_cat.click();
                }
                while (helper.swipeToElementByText("//*[contains(@text, '" + target_cat + "')]")) {
                    helper.swipeInDirection(direction.UP, "up", 0.8);
                    target_cat.click();
                }

            } catch (Exception f) {
                while (helper.swipeToElementByText("//*[contains(@text, '" + first_cat + "')]")) {
                    helper.swipeInDirection(direction.UP, "up", 0.8);
                    first_cat.click();
                }
                while (helper.swipeToElementByText("//*[contains(@text, '" + second_cat + "')]")) {
                    helper.swipeInDirection(direction.UP, "up", 0.8);
                    second_cat.click();
                }
                while (helper.swipeToElementByText("//*[contains(@text, '" + target_cat + "')]")) {
                    helper.swipeInDirection(direction.UP, "up", 0.8);
                    target_cat.click();
                }
            }
        }

        System.out.println("Wybrano kategorię ogłoszenia");
    }

    public void setPrivacy(boolean privacy) {

        if (privacy) {
            btn_private.click();
        } else {
            btn_company.click();
            System.out.println("Wybrano prywatność");
        }
    }

    public void setCondition(boolean used) {

        while (helper.swipeToElementByText("Cena do negocjacji?")) {
            helper.swipeInDirection(direction.UP, "up", 0.5);
        }

        if (used) {
            btn_conditionUsed.click();
        } else {
            btn_conditionNew.click();
        }

        while (helper.swipeToElementByText("Opis")) {
            helper.swipeInDirection(direction.UP, "up", 0.4);
        }

        System.out.println("Wybrano stan przedmiotu");
    }


    public void setPrice(String price) {
        input_enterThePrice.click();
        input_enterThePrice.sendKeys(price);

        helper.pressAndroidBackBtn();

        while (helper.swipeToElementByText("Kontakt do Ciebie")) {
            helper.swipeInDirection(direction.UP, "up", 0.4);
        }
        System.out.println("Ustalono cenę przedmiotu");
    }

    public void negotiatingPrice() {
        btn_priceIsNegiotiable.click();

        System.out.println("Ustalono czy cena ma być negocjonowana");
    }

    public void addDescription(String description) {
        input_addDescription.click();
        input_addDescription.sendKeys(description);
        helper.pressAndroidBackBtn();

        while (helper.swipeToElementByText("Zobacz przed dodaniem")) {
            helper.swipeInDirection(direction.UP, "up", 0.8);
        }
        System.out.println("Dodano opis przedmiotu aukcji");
    }

    public void addPhoneNumber(String phoneNumber) {
        input_addPhoneNumber.click();
        input_addPhoneNumber.sendKeys(phoneNumber);
        helper.pressAndroidBackBtn();

        System.out.println("Dodano numer kontaktowy");
    }

    public void addAdvert(String title) {
        btn_seeBeforeAdding.click();
        while (helper.swipeToElementByText("Wróć")) {
            helper.swipeInDirection(direction.UP, "up", 0.4);
        }
        btn_adAdd.click();
        btn_adWithoutPromotion.click();

        helper.getScreenShot("afterAdingAddWithPhoto.png");

        btn_backToHomePage.click();

        System.out.println("Dodano ogłoszenie" + title);
    }


}


//endregion METODY








