package com.selenium.java.pages.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.Helper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class RegistrationScreen extends BaseTest {


    //region PRZYCISKI
    @FindBy(how = How.ID, using = "pl.tablica:id/createAccountBtn")
    public WebElement btn_register;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'E-mail')]")
    public WebElement input_email;

    @FindBy(how = How.ID, using = "pl.tablica:id/input")
    public WebElement input_password;

    @FindBy(how = How.ID, using = "pl.tablica:id/checkbox")
    public WebElement chbx_acceptConditions;

    @FindBy(how = How.ID, using = "pl.tablica:id/btnLogInNew")
    public WebElement btn_createAccount;


    //region METODY
    Helper helper = new Helper();
    public void registerToApp(String email, String password) {

        btn_register.click();
        System.out.println("Wybrano opcję zarejestruj");

        input_email.sendKeys(email);
        System.out.println("Wpisano email: " + email);
        input_password.sendKeys(password);
        System.out.println("Wpisano hasło: " + password);

        while (helper.swipeToElementById("pl.tablica:id/btnLogInNew")) {
            helper.swipeInDirection(direction.UP, "up", 0.6);
        }
    }


    public void createAccount() {

        chbx_acceptConditions.click();
        System.out.println("Zaakceptowano warunki rejestracji");
        btn_createAccount.click();
        System.out.println("Wybrano przycisk założenia konta w OLX");

    }

}


//endregion METODY



