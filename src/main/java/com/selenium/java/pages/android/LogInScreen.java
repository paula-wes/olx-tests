package com.selenium.java.pages.android;

import com.selenium.java.base.BaseTest;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;



public class LogInScreen extends BaseTest {

    @FindBy(how = How.ID, using = 	"pl.tablica:id/loginBtn")
    public WebElement btn_loginToApp;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'E-mail')]")
    public WebElement input_email;

    @FindBy(how = How.ID, using = "pl.tablica:id/input")
    public WebElement input_password;

    @FindBy(how = How.ID, using = "pl.tablica:id/btnLogInNew")
    public WebElement btn_login;

    @FindBy(how = How.ID, using = "pl.tablica:id/closeButton")
    public WebElement btn_closeScreen;



    public void loginToApp(String email, String password) {

        btn_loginToApp.click();
        System.out.println("Wybrano opcję logowania");

        input_email.sendKeys(email);
        System.out.println("Wpisano adres email: " + email);
        input_password.sendKeys(password);
        System.out.println("Wpisano hasło: " + password);
//        btn_login.isDisplayed();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        btn_login.click();
        System.out.println("Wciśnięto przycisk logowania");

    }

    public void dismissLogInPop(){
        btn_closeScreen.click();
        System.out.println("Zamknięto pop-up");
    }

    }



