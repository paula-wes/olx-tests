package com.selenium.java.pages.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.Helper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.sql.SQLOutput;

public class CategoryScreen extends BaseTest {

    @FindBy(how = How.ID, using = "pl.tablica:id/seeAllCategories")
    public WebElement btn_seeAllCategories;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Dla Dzieci')]")
    public WebElement btn_ForKidsCategory;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Ubranka dla dzieci')]")
    public WebElement btn_clothesForKids;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Filtruj')]")
    public WebElement btn_filter;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/" +
            "android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view." +
            "ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android." +
            "widget.LinearLayout/android.widget.LinearLayout[3]/android.view.ViewGroup/android.widget.LinearLayout[2]" +
            "/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText")
    public WebElement btn_location;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Pomorskie')]")
    public WebElement btn_chooseVoivodeship;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Gdańsk')]")
    public WebElement btn_chooseCity;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/" +
            "android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view." +
            "ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.view.ViewGroup/" +
            "android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ImageButton[2]")
    public WebElement btn_addDistance;

    @FindBy(how = How.ID, using = "pl.tablica:id/fromField")
    public WebElement btn_priceFrom;

    @FindBy(how = How.ID, using = "pl.tablica:id/fromField")
    public WebElement input_priceFrom;

    @FindBy(how = How.ID, using = "pl.tablica:id/toField")
    public WebElement input_priceTo;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Chłopięce')]")
    public WebElement btn_typeChlopiece;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Używane')]")
    public WebElement btn_conditionUsed;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Zamienię')]")
    public WebElement btn_conditionExchange;

    @FindBy(how = How.ID, using = "pl.tablica:id/chooserBtn")
    public WebElement btn_size;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, '116')]")
    public WebElement btn_chooseSize1;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, '128')]")
    public WebElement btn_chooseSize2;

    @FindBy(how = How.ID, using = "pl.tablica:id/btnSubmit")
    public WebElement btn_submit;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Prywatne')]")
    public WebElement btn_choosePrivate;

    @FindBy(how = How.ID, using = "pl.tablica:id/view_type_gallery")
    public WebElement btn_chooseGalleryView;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Pokaż wyniki')]")
    public WebElement btn_showResults;

    @FindBy(how = How.ID, using = "pl.tablica:id/observeBtn")
    public WebElement btn_observeItem;

    Helper helper = new Helper();
    public void setCategories() {

        btn_seeAllCategories.click();

        while (helper.swipeToElementByText("Dla Dzieci")) {
            helper.swipeInDirection(direction.UP, "up", 0.6);
        }

        btn_ForKidsCategory.click();
        btn_clothesForKids.click();

        System.out.println("Wybrano kategorię");
    }

    public void filterLocation() {
        btn_filter.click();
        btn_location.click();

        while (helper.swipeToElementByText("Pomorskie")) {
            helper.swipeInDirection(direction.UP, "up", 0.8);
        }

        btn_chooseVoivodeship.click();
        btn_chooseCity.click();
        btn_addDistance.click();
        btn_addDistance.click();

        System.out.println("Filtrowanie parametrów");
    }

    public void setPrice(String priceFrom, String priceTo) {
        btn_priceFrom.click();

        input_priceFrom.sendKeys(priceFrom);
        input_priceTo.sendKeys(priceTo);
        helper.pressAndroidBackBtn();
        System.out.println("Ustalono cenę");
    }

    public void setConditions() {
        while (helper.swipeToElementByText("Firmowe")) {
            helper.swipeInDirection(direction.UP, "up", 0.6);
        }

        btn_typeChlopiece.click();

        btn_conditionUsed.click();
        btn_conditionExchange.click();
        btn_size.click();

        while (helper.swipeToElementByText("128")) {
            helper.swipeInDirection(direction.UP, "up", 0.8);
        }

        btn_chooseSize1.click();
        btn_chooseSize2.click();
        System.out.println("Ustalono stan, typ, rozmiar");
    }

    public void addAdv() {
        btn_submit.click();
        btn_choosePrivate.click();

        helper.swipeInDirection(direction.UP, "up", 0.8);

        btn_chooseGalleryView.click();
        btn_showResults.click();

        helper.getScreenShot("categoryscreen.png");

        System.out.println("Ukazano rezultat wyszukiwania");

    }
}
