package com.selenium.java.tests.presentation;


public class CompareStringsAndIfElse {

    public static void main(String[] args) {
        compareStrings();
        compareStrings_Positive();
        compareStrings_Negative();
        compareStrings_Positive_2();
        compareStrings_Negative_2();

    }

    public static void compareStrings_Positive() {
        String object1 = "com/selenium/java/tests/test";
        String object2 = "com/selenium/java/tests/test";


        if (object1.equals(object2)) {
            System.out.println("Works as expected!");
        } else {
            System.out.println("Nope...");
        }
    }

    public static void compareStrings_Positive_2() {
        String object1 = "com/selenium/java/tests/test";
        String object2 = "com/selenium/java/tests/test";


        if (object1 == object2) { // nie zaleca się stosować == w Stringach
            System.out.println("Works as expected!");
        } else {
            System.out.println("Nope...");
        }
    }

    public static void compareStrings_Negative() {
        String object1 = "com/selenium/java/tests/test";
        String object2 = "test_2";


        if (object1.equals(object2)) {
            System.out.println("Works as expected!");
        } else {
            System.out.println("Nope...");
        }
    }

    public static void compareStrings_Negative_2() {
        String object1 = "com/selenium/java/tests/test";
        String object2 = "test_2";


        if (!object1.equals(object2)) {
            System.out.println("Works as expected!");
        } else {
            System.out.println("Nope...");
        }
    }

    public static void compareStrings() {
        String object1 = "com/selenium/java/tests/test";
        String object2 = "test2";
        String object3 = "test3";

        if (object1.equals(object2)) {
            System.out.println("object 1 = object2");
        } else if (object1.equals(object3)) {
            System.out.println("object1 = object 3");
        } else if (object2.equals(object3)) {
            System.out.println("object2 = object 3");
        } else {
            System.out.println("Nope... Nope...");
        }
    }

}

