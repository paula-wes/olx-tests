package com.selenium.java.tests.presentation;

public class Switch {
    private enum Colors {
        BLUE,
        RED,
        YELLOW,
        PINK,
        NONE
    }

    public static void main(String[] args) {
        ChooseColor(Colors.RED);
        ChooseColor(Colors.BLUE);
        ChooseColor(Colors.NONE);
    }

        public static void ChooseColor (Colors colors){
            switch (colors) {

                case RED:
                    System.out.println("Yay! It's red!");
                    break;
                case BLUE:
                    System.out.println("Yay! It's blue!");
                    break;
                case YELLOW:
                    System.out.println("Yay! It's yellow!");
                    break;
                case PINK:
                    System.out.println("Yay! It's pink!");
                    break;
                default:
                    System.out.println("Nooooneee...");



            }
        }

    }

