package com.selenium.java.tests.presentation;

public class Loops {

    public static void main(String[] args) {
//        whileLoop();
//        doWhileLoop();
//        forLoop();
        foreachLoop();
    }


    //region WHILE
    public static void whileLoop() {

        int licznik = 0;
        while (licznik < 10) {
            System.out.println("While Loop " + licznik);

            licznik++;
        }
        System.out.println("End of While Loop");
    }
    //endregion WHILE


    //region DO WHILE
    public static void doWhileLoop() {

        int licznik = 0;
        do {
            System.out.println("This is Loop " + licznik);
            licznik++;
        }
        while (licznik < 10);
        System.out.println("End of Do While Loop");
    }


    //endregion DO WHILE


    //region FOR
    public static void forLoop() {
        for (int i = 0; i < 10; i++) {
            System.out.println("This is FOR Loop " + i);
        }
        System.out.println("End of Loop");
    }


    //endregion FOR


    //region FOREACH
    public static void foreachLoop() {

        int[] tablica = new int[10];

        for (int i = 0; i < 10; i++) {
            tablica[i] = i + 1;
        }
        for (int x : tablica) {
            System.out.println(x);
        }

    }


    //endregionFOREACH
}
