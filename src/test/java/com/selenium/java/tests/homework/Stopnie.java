package com.selenium.java.tests.homework;

import com.selenium.java.tests.homework.Osoba;

public class Stopnie {
    public static void main(String[] args) {
        Osoba marekw = new Osoba();
        Osoba darekp = new Osoba();
        Osoba piotrw = new Osoba();


        marekw.srednia("Marek Waszak", new double[]{4, 3.5, 5, 3.5, 4, 5, 6, 3});
        darekp.srednia("Darek Pyrek", new double[]{2, 2.5, 5, 6, 1, 3.5, 4.5, 2});
        piotrw.srednia("Piotr Wąsik", new double[]{6, 6, 6, 4, 3.5, 3, 1, 1, 1.5, 5, 6});

    }
}
