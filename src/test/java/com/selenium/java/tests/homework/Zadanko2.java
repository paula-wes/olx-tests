package com.selenium.java.tests.homework;

import java.util.Arrays;

public class Zadanko2 {
    private enum Imiona {
        KASIA, ASIA, ANTONI, ZYGMUNT, FILIP, WIOLA, PATRYK, OLA,
        PATRYCJA, ROBERT

    }


    public static void main(String[] args) {

        String[] listaImion = new String[Imiona.values().length];
        int i = 0;
        for (Imiona imie : Imiona.values()) {
            listaImion[i++] = imie.name();
        }


        System.out.println("Lista imion: ");
        for (String name : listaImion)
            System.out.println(name);


        // Wyświetlanie posortowanej listy imion
        Arrays.sort(listaImion);
        System.out.println("\n\nPosortowana lista imion:");
        for (String name : listaImion)
            System.out.println(name);


        // Wyświetlanie rozmiaru tablicy:
        System.out.println("Rozmiar tablicy wynosi: " + listaImion.length);

    }
}

/*
Zadanie 2:
Stwórz tablice Stringów 10-elementową, w której umieścisz 10 wybranych za pomocą ENUMA imion.
Wyświetl wszystkie elementy z tablicy.
Wyświetl rozmiar tablicy.
Posegreguj alfabetycznie i wyświetl imiona w tablicy.
 */
