package com.selenium.java.tests.homework;

import java.util.Arrays;

public class Cwicz {

    public enum Names {
        KASIA, ASIA, ANTONI, ZYGMUNT, FILIP, WIOLA, PATRYK, OLA,
        PATRYCJA, ROBERT
    }

    public static String PokazImie(Names imiona) {
        String imie;
        switch (imiona) {

            case KASIA:
                imie = "Kasia";
                break;
            case ASIA:
                imie = "Asia";
                break;
            case ANTONI:
                imie = "Antoni";
                break;
            case ZYGMUNT:
                imie = "Zygmunt";
                break;
            case FILIP:
                imie = "Filip";
                break;
            case WIOLA:
                imie = "Wiola";
                break;
            case PATRYK:
                imie = "Patryk";
                break;
            case OLA:
                imie = "Ola";
                break;
            case PATRYCJA:
                imie = "Patrycja";
                break;
            case ROBERT:
                imie = "Robert";
                break;
            default:
                imie = "none";
        }
        return imie;
    }


    public static void main(String[] args) {
        String listaImion[] = {
                PokazImie(Names.KASIA),
                PokazImie(Names.ASIA),
                PokazImie(Names.ANTONI),
                PokazImie(Names.ZYGMUNT),
                PokazImie(Names.FILIP),
                PokazImie(Names.WIOLA),
                PokazImie(Names.PATRYK),
                PokazImie(Names.OLA),
                PokazImie(Names.PATRYCJA),
                PokazImie(Names.ROBERT)

        };


        for (String imie : listaImion) {
            System.out.println(imie);
        }

        System.out.println("\n\nLista imion alfabetycznie: ");
        Arrays.sort(listaImion);

        for (String sort : listaImion) {
            System.out.println(sort);
        }
        // Wyświetlanie rozmiaru tablicy:
        System.out.println("\n\nRozmiar tablicy wynosi: " + listaImion.length);

    }
}

/*
Zadanie 2:
Stwórz tablice Stringów 10-elementową, w której umieścisz 10 wybranych za pomocą ENUMA imion.
Wyświetl wszystkie elementy z tablicy.
Wyświetl rozmiar tablicy.
Posegreguj alfabetycznie i wyświetl imiona w tablicy.
 */