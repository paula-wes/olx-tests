package com.selenium.java.tests.test.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.Helper;
import com.selenium.java.pages.android.LogInScreen;
import com.selenium.java.pages.android.RegistrationScreen;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;

public class RegistrationAndroidTest extends BaseTest {

    @BeforeClass(description = "Starting appium server")
    public void startup() {
        startAppiumServer();
    }

    @BeforeMethod
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceId, String deviceName, ITestContext context) {
        instalApp = false;
        launchAndroid(platform, deviceId, deviceName, context);
    }


    //region DATAPROVIDERY
    @DataProvider
    public Object[][] logInToApp() {
        return new Object[][]{
                {"paula_testuje@onet.pl", "Tester2020OSW"},
                {"paula_testuje1@onet.pl", "Tester2020OSW"},
                {"paula_testuje2@onet.pl", "Tester2020OSW"},
                {"paula_testuje3@onet.pl", "Tester2020OSW"},
                {"paula_testuje4@onet.pl", "Tester2020OSW"}
        };
    }

    @DataProvider
    public Object[][] withoutEmail() {
        return new Object[][]{
                {"", "Tester2020OSW"},
                {"", "Tester2020OSW"},
                {"", "Tester2020OSW"},
                {"", "Tester2020OSW"},
                {"", "Tester2020OSW"}
        };
    }
    @DataProvider
    public Object[][] weakPassword() {
        return new Object[][]{
                {"paula_testuje@onet.pl", "ala"},
                {"paula_testuje1@onet.pl", "123"},
                {"paula_testuje2@onet.pl", "mama"},
                {"paula_testuje3@onet.pl", "ma1"},
                {"paula_testuje4@onet.pl", "12345"}

        };
    }

    @DataProvider
    public Object[][] withoutPassword() {
        return new Object[][]{
                {"paula_testuje@onet.pl", ""},
                {"paula_testuje1@onet.pl", ""},
                {"paula_testuje2@onet.pl", ""},
                {"paula_testuje3@onet.pl", ""},
                {"paula_testuje4@onet.pl", ""}
        };
    }

    @DataProvider
    public Object[][] wrongEmailAddress() {
        return new Object[][]{
                {"paula", "Tester2020OSW"},
                {"paula_", "Tester2020OSW"},
                {"paula_testuje", "Tester2020OSW"},
                {"paula_testuje@", "Tester2020OSW"},
                {"paula_testuje@wp", "Tester2020OSW"}
        };
    }

    @DataProvider
    public Object[][] accountExist() {
        return new Object[][]{
                {"paula_testuje@onet.pl", "Tester"},
                {"paula_testuje1@onet.pl", "tester2020OSW"},
                {"paula_testuje2@onet.pl", "Tester2020"},
                {"paula_testuje3@onet.pl", "TesterOSW"},
                {"paula_testuje4@onet.pl", "TesterOSW2020"}
        };
    }

    //endregion DATAPROVIDERY

    //region TESTS

    Helper helper = new Helper();
    @Severity(SeverityLevel.MINOR)
    @Test(dataProvider ="logInToApp", description = "Checking basic functionality such as logging in with correct data")
    public void testLogIn(String email, String password)
    {
        System.out.println("Test logowania się na założone już konto");

        WebDriver driver = getDriver();
        LogInScreen logInScreen = PageFactory.initElements(driver, LogInScreen.class);

        logInScreen.loginToApp(email, password);

        Assert.assertFalse(logInScreen.btn_login.getText().equals("Witaj"), "Błędne logowanie.");
        System.out.println("Zalogowano do aplikacji OLX za pomocą maila " + email);

        helper.getScreenShot("LogIn OLX Android Test.png");

    }
    @Severity(SeverityLevel.MINOR)
    @Test(dataProvider ="withoutEmail", description = "Checking basic registration functions, i.e. without an email address")
    public void testNoEmail(String email, String password)
    {
        System.out.println("Test rejestrowania się bez użycia adresu mailowego");

        WebDriver driver = getDriver();
        RegistrationScreen regiScreen = PageFactory.initElements(driver, RegistrationScreen.class);

        regiScreen.registerToApp(email, password);
        regiScreen.createAccount();

        Assert.assertFalse(regiScreen.btn_createAccount.getText().equals("Witaj"), "Nie udało się zarejestrować konta.");
        System.out.println("Nie udało się zarejestrować do aplikacji OLX za pomocą maila " + email);

        helper.getScreenShot("Registration OLX Android Test - no email.png");
    }

    @Severity(SeverityLevel.MINOR)
    @Test(dataProvider ="weakPassword", description = "Checking basic registration functions, i.e. with weak password")
    public void testWeakPassword(String email, String password)
    {
        System.out.println("Test rejestrowania się z wykorzystaniem słabego hasła");

        WebDriver driver = getDriver();
        RegistrationScreen regiScreen = PageFactory.initElements(driver, RegistrationScreen.class);

        regiScreen.registerToApp(email, password);
        regiScreen.createAccount();

        Assert.assertFalse(regiScreen.btn_createAccount.getText().equals("Witaj"), "Nie udało się zarejestrować konta.");
        System.out.println("Nie udało się zarejestrować do aplikacji OLX za pomocą maila " + email);

        helper.getScreenShot("Registration OLX Android Test - weak pass.png");
    }

    @Severity(SeverityLevel.MINOR)
    @Test(dataProvider ="withoutPassword", description = "Checking basic registration functions, i.e. without password")
    public void testNoPassword(String email, String password)
    {
        System.out.println("Test rejestrowania się bez wpisania hasła");

        WebDriver driver = getDriver();
        RegistrationScreen regiScreen2 = PageFactory.initElements(driver, RegistrationScreen.class);

        regiScreen2.registerToApp(email, password);
        regiScreen2.createAccount();

        Assert.assertFalse(regiScreen2.btn_createAccount.getText().equals("Witaj"), "Nie udało się zarejestrować konta.");
        System.out.println("Nie udało się zarejestrować do aplikacji OLX za pomocą maila " + email);

        helper.getScreenShot("Registration OLX Android Test - weak pass.png");
    }

    @Severity(SeverityLevel.MINOR)
    @Test(dataProvider ="wrongEmailAddress", description = "Checking basic registration functions, i.e. with wrong email address")
    public void testWrongEmail(String email, String password)
    {
        System.out.println("Test rejestrowania się z użyciem błędnego adresu mailowego");

        WebDriver driver = getDriver();
        RegistrationScreen regiScreen3 = PageFactory.initElements(driver, RegistrationScreen.class);

        regiScreen3.registerToApp(email, password);
        regiScreen3.createAccount();

        Assert.assertFalse(regiScreen3.btn_createAccount.getText().equals("Witaj"), "Nie udało się zarejestrować konta.");
        System.out.println("Nie udało się zarejestrować do aplikacji OLX za pomocą maila " + email);

        helper.getScreenShot("Registration OLX Android Test - weak pass.png");
    }

    @Severity(SeverityLevel.MINOR)
    @Test(dataProvider ="accountExist", description = "Checking the basic registration functionalities for an account already created" )
    public void accountExist(String email, String password)
    {
        System.out.println("Test rejestrowania się z wpisaniem błędnego hasła");

        WebDriver driver = getDriver();
        RegistrationScreen regiScreen4 = PageFactory.initElements(driver, RegistrationScreen.class);

        regiScreen4.registerToApp(email, password);
        regiScreen4.createAccount();

        Assert.assertFalse(regiScreen4.btn_createAccount.getText().equals("Witaj"), "Nie udało się zarejestrować konta.");
        System.out.println("Nie udało się zarejestrować do aplikacji OLX za pomocą maila " + email);

        helper.getScreenShot("Registration OLX Android Test - weak pass.png");
    }

    //endregion TESTS
    @AfterMethod
    public void tearDown2() {
        System.out.println("Test zakończono");
    }

}