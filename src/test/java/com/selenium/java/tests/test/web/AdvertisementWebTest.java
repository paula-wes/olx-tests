package com.selenium.java.tests.test.web;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.WebHelper;
import com.selenium.java.listeners.TestListener;
import com.selenium.java.pages.web.AdvertisementPage;
import com.selenium.java.pages.web.HomePage;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.*;
import java.awt.*;
import java.net.MalformedURLException;

import static org.testng.Assert.assertTrue;

@Listeners({TestListener.class})
public class AdvertisementWebTest extends BaseTest {

    @BeforeMethod(description = "Starting appium server")
    @Parameters(value = {"Platform", "Browser"})
    public void setUp(String platform, String browser, ITestContext context) {

        headless = false;// dotyczy tego jak otwierają się testy w przeglądarce
        url = "https://www.olx.pl";

        launchWeb(platform, browser, context);
        System.out.println("Testy rozpoczęto");
    }

    //region DATAPROVIDERS
    @DataProvider
    public Object[][] advertWithoutPhoto() {
        return new Object[][]{
                {"testowanie_aplikacji@wp.pl", "Tester2020OSW", "Mapex - Perkusja Storm Rock ST5295F", "Instrumenty",
                        "Muzyka i Edukacja", false, "sell", "3000", true, "Sprzedam perksuję tjw. Osoby zainteresowane zapraszam " +
                        "do kontaktu", "85-800"},
                {"testowanie_aplikacji@wp.pl", "Tester2020OSW", "Torebka damska Guess", "Torebki",
                        "Moda", true, "change", "", true, "Wymienię torebkę tjw. Osoby zainteresowane zapraszam " +
                        "do kontaktu", "00-010"},
                {"testowanie_aplikacji@wp.pl", "Tester2020OSW", "Fotelik Axkid Minikid 2.0", "Foteliki - Nosidełka",
                        "Dla Dzieci", false, "sell", "1800", false, "Sprzedam fotelik samochodowy tjw. Osoby zainteresowane zapraszam " +
                        "do kontaktu", "83-200"}
        };
    }

    @DataProvider
    public Object[][] advertPhoto() {
        return new Object[][]{
                {"testowanie_aplikacji@wp.pl", "Tester2020OSW", "Mapex - Perkusja Storm Rock ST5295F", "Instrumenty",
                        "Muzyka i Edukacja", false, "sell", "3000", true, "Sprzedam perksuję tjw. Osoby zainteresowane zapraszam " +
                        "do kontaktu","/Users/paula/Pictures/perkusja.jpg", "85-800"},
                {"testowanie_aplikacji@wp.pl", "Tester2020OSW", "Torebka damska Guess", "Torebki",
                        "Moda", true, "change", "", true, "Wymienię torebkę tjw. Osoby zainteresowane zapraszam " +
                        "do kontaktu", "/Users/paula/Pictures/torebka.jpg", "00-010"},
                {"testowanie_aplikacji@wp.pl", "Tester2020OSW", "Fotelik Axkid Minikid 2.0", "Foteliki - Nosidełka",
                        "Dla Dzieci", false, "sell", "1800", false, "Sprzedam fotelik samochodowy tjw. Osoby zainteresowane zapraszam " +
                        "do kontaktu","/Users/paula/Pictures/fotelik.png", "83-200"}

        };
    }
    //endregion DATAPROVIDERS

    //region TEST
    WebHelper helper = new WebHelper();

    @Severity(SeverityLevel.MINOR)
    @Test(dataProvider = "advertWithoutPhoto", description = "Checking the basic functionalities i.e. adding an advertisement")
    public void advertWithoutPhotoTest(String email, String pass, String title, String targetCat, String firstCat,
                                       boolean used, String saleMethod, String price, boolean priv,
                                       String description, String location) {

        System.out.println("Rozpoczęto test sprawdzający funkcję dodawania ogłoszenia bez zdjęcia");

        WebDriver driver = getDriver();
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        AdvertisementPage advPage = PageFactory.initElements(driver, AdvertisementPage.class);

        homePage.closeCookies();
        homePage.loginToApp(email, pass);
        advPage.addAdvert();
        advPage.setTitle(title);
        advPage.chooseCategory(targetCat, firstCat);
        advPage.setSalesMethod(saleMethod, price);
        advPage.setUsedOrNew(used);
        advPage.setPrivacy(priv);
        advPage.setDescription(description);
        advPage.setContactData(location);
        advPage.checkBeforeAdding();
        advPage.addReadyAdvert();
        helper.testScreenShoot("AdvertisementWebTest");

        assertTrue(driver.findElement(By.className("confirm-polaroid__title"))
                .getText().equals("Dziękujemy za dodanie ogłoszenia"));
        System.out.println("Test zaliczony - dodano nowe ogłoszenie");

   //     helper.getScreenShot("Account OLX Web Test - advert.png");
    }

    @Severity(SeverityLevel.MINOR)
    @Test(dataProvider = "advertPhoto", description = "Checking the basic functionalities i.e. adding an advertisement with a photo")
    public void advertPhotoTest(String email, String pass, String title, String targetCat, String firstCat,
                                boolean used, String saleMethod, String price, boolean priv,
                                String description, String path, String location) throws AWTException, MalformedURLException {

        System.out.println("Rozpoczęto test sprawdzający funkcję dodawania ogłoszenia bez zdjęcia");

        WebDriver driver = getDriver();
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        AdvertisementPage advPage = PageFactory.initElements(driver, AdvertisementPage.class);

        homePage.closeCookies();
        homePage.loginToApp(email, pass);
        advPage.addAdvert();
        advPage.setTitle(title);
        advPage.chooseCategory(targetCat, firstCat);
        advPage.setSalesMethod(saleMethod, price);
        advPage.setUsedOrNew(used);
        advPage.setPrivacy(priv);
        advPage.setDescription(description);
        advPage.addPhotos(path);
        advPage.setContactData(location);
        advPage.checkBeforeAdding();
        advPage.addReadyAdvert();
        helper.testScreenShoot("AdvertisementWebTest");

        assertTrue(driver.findElement(By.className("confirm-polaroid__title"))
                .getText().equals("Dziękujemy za dodanie ogłoszenia"));
        System.out.println("Test zaliczony - dodano nowe ogłoszenie");

       // helper.getScreenShot("Account OLX Web Test - photo advert.png");
    }

    //endregion TESTS

    @AfterMethod
    public void tearDown() {

        WebDriver driver = getDriver();

        System.out.println("Test zakończono");
        driver.close();
    }
}
