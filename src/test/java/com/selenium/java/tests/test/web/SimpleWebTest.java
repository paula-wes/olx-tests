package com.selenium.java.tests.test.web;

import com.selenium.java.base.BaseTest;
import com.selenium.java.pages.web.HomePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class SimpleWebTest extends BaseTest {

    @BeforeMethod
    @Parameters(value = {"Platform", "Browser"})
    public void setUp(String platform, String browser, ITestContext context) {

        headless = false;; // dotyczy tego jak otwierają się testy w przeglądarce
        url = "https://www.olx.pl";


        launchWeb(platform, browser, context);
        System.out.println("Test started");
    }

    @Test
    public void simpleTest(){

        WebDriver driver = getDriver();
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
       // homePage.loginToApp();


    }

//    @AfterMethod
//    public void tearDown(){
//
//        WebDriver driver = getDriver();
//
//        System.out.println("Test ended");
//        driver.close();
//    }

}
