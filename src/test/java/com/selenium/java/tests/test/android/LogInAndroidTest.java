package com.selenium.java.tests.test.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.Helper;
import com.selenium.java.pages.android.LogInScreen;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;
import static org.testng.Assert.assertFalse;


public class LogInAndroidTest extends BaseTest {

    @BeforeClass(description = "Starting appium server")
    public void startup() {
        startAppiumServer();
    }

    @BeforeMethod
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceId, String deviceName, ITestContext context) {
        instalApp = false;

        launchAndroid(platform, deviceId, deviceName, context);
    }

    @DataProvider
    public Object[][] wrongPassword() {
        return new Object[][]{
                {"paula_testuje@onet.pl", "Tester2020"},
                {"paula_testuje1@onet.pl", "ster2020OSW"},
                {"paula_testuje2@onet.pl", "tester2020OSW"},
                {"paula_testuje3@onet.pl", "TesterOSW"},
                {"paula_testuje4@onet.pl", "Tester"}
        };

    }

    @DataProvider
    public Object[][] wrongEmail() {
        return new Object[][]{
                {"paula_testuje@onet.", "Tester2020"},
                {"paula_testuje1@", "ster2020OSW"},
                {"@onet.pl", "tester2020OSW"},
                {"paula_testuje3@onet@.pl", "TesterOSW"},
                {"paula_testuje4@.pl", "Tester"}
        };
    }

    @DataProvider
    public Object[][] properLogIn() {
        return new Object[][]{
                {"paula_testuje@onet.pl", "Tester2020OSW"},
                {"paula_testuje1@onet.pl", "Tester2020OSW"},
                {"paula_testuje2@onet.pl", "Tester2020OSW"},
                {"paula_testuje3@onet.pl", "Tester2020OSW"},
                {"paula_testuje4@onet.pl", "Tester2020OSW"}
        };
    }

    Helper helper = new Helper();

    @Severity(SeverityLevel.MINOR)
    @Test(dataProvider = "wrongPassword", description = "Checking basic functionality logging in with wrong password")
    public void setWrongPass(String email, String password) {
        System.out.println("Rozpoczęcie testu logowania się do aplikacji z wpisaniem błędnego hasła");

        WebDriver driver = getDriver();
        LogInScreen logInScreen = PageFactory.initElements(driver, LogInScreen.class);

        logInScreen.loginToApp(email, password);

        assertFalse(logInScreen.btn_login.getText().equals("Witaj!"), "Niepoprawne logowanie się");
        System.out.println("Niezalogowano się do aplikacji OLX za pomocą maila " + email);

        helper.getScreenShot("LogIn OLX Android Test - wrong pass.png");
    }

    @Severity(SeverityLevel.MINOR)
    @Test(dataProvider = "wrongEmail", description = "Checking basic functionality logging in with wrong e-mail address")
    public void setWrongEmail(String email, String password) {
        System.out.println("Rozpoczęto test logowania się do aplikacji z wpisaniem błędnego adresu e-mail");

        WebDriver driver = getDriver();
        LogInScreen logInScreen = PageFactory.initElements(driver, LogInScreen.class);


        logInScreen.loginToApp(email, password);

//        Assert.assertFalse(logInScreen.btn_login.getText().equals("Witaj!"), "Niepoprawne logowanie się");
//        System.out.println("Niezalogowano się do aplikacji OLX za pomocą maila " + email);
        Assert.assertTrue(driver.findElement(By.id("pl.tablica:id/btnLogInNew")).isDisplayed());
        System.out.println("Test zaliczony - nie zalogowano użytkownika");

        helper.getScreenShot("LogIn OLX Android Test - wrong email.png");
    }

    @Severity(SeverityLevel.MINOR)
    @Test(dataProvider = "properLogIn", description = "Checking basic functionality logging in with correct data")
    public void setProperPass(String email, String password) {
        System.out.println("Rozpoczeto test logowania się z użyciem poprawnych danych");

        WebDriver driver = getDriver();
        LogInScreen logInScreen = PageFactory.initElements(driver, LogInScreen.class);


        logInScreen.loginToApp(email, password);

//        Assert.assertFalse(logInScreen.btn_login.getText().equals("Witaj!"), "Niepoprawne logowanie się");
//        System.out.println("Niezalogowano się do aplikacji OLX za pomocą maila " + email);

        Assert.assertTrue(driver.findElement(By.id("pl.tablica:id/btnLogInNew")).isDisplayed());
        System.out.println("Test zaliczony - nie zalogowano użytkownika");
        helper.getScreenShot("LogIn OLX Android Test - proper log in.png");

    }
    @Test
    public void test(){

    }

    @AfterMethod
    public void teardown2() {
        System.out.println("Test zakończono.");
    }
}
