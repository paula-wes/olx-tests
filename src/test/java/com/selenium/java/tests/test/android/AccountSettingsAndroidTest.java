package com.selenium.java.tests.test.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.Helper;
import com.selenium.java.pages.android.AccountScreen;
import com.selenium.java.pages.android.LogInScreen;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;

import static org.testng.Assert.assertTrue;

public class AccountSettingsAndroidTest extends BaseTest {

    @BeforeClass
    public void startup() {
        startAppiumServer();
    }

    @BeforeMethod
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceId, String deviceName, ITestContext context) {

        instalApp = false;
        launchAndroid(platform, deviceId, deviceName, context);
    }


    @DataProvider
    public Object[][] passToShort() {
        return new Object[][]{
                {"Tester2020OSW", "ala"},
                {"Tester2020OSW", "123"},
                {"Tester2020OSW", "aaa"},
        };
    }

    @DataProvider
    public Object[][] wrongOldPass() {
        return new Object[][]{
                {"Tester", "test1szy"},
                {"Tester2020", "test2gi"},
                {"TesterOSW", "test3ci"},
        };
    }

    @DataProvider
    public Object[][] wrongEmail() {
        return new Object[][]{
                {"paula@"},
                {"tester@wp"},
                {"tester@wp."},

        };
    }

    @DataProvider
    public Object[][] usedEmail() {
        return new Object[][]{
                {"paula_testuje@onet.pl"},
                {"paula_testuje1@onet.pl"},
                {"paula_testuje2@onet.pl"},

        };
    }

    @DataProvider
    public Object[][] editProfile() {
        return new Object[][]{
                {"Paula Anna"},
                {"P"},
                {"123"},
                {"Paula"}
        };
    }

    @DataProvider
    public Object[][] invoiceSett() {
        return new Object[][]{
                {"", "Brzozowa 88", "88-888", "Brzozowo", "8888888888", false},
                {"Tester sp. z o.o.", "", "88-888", "Brzozowo", "8888888888", false},
                {"Tester sp. z o.o.", "Brzozowa 88", "", "Brzozowo", "8888888888", false},
                {"Tester sp. z o.o.", "Brzozowa 88", "88-888", "", "8888888888", false},
                {"Tester sp. z o.o.", "Brzozowa 88", "88-888", "Brzozowo", "", true},
                //  {"Tester sp. z o.o.","Brzozowa 88", "88-888", "Brzozowo", "8888888888"},
        };
    }

    @DataProvider
    public Object[][] contactOLX() {
        return new Object[][]{
                {"paula_testuje@wp.pl", "", "4354345"},
                {"paula_testuje1@wp.pl", "Pomimo, że na dany numer telefonu nie jest przypisana aplikacja OLX nie mogę skorzystać z tego nr przy zmianie. Co można zrobić, żeby to rozwiązać?", ""},
                {"", "", "4354345"},
                {"paula_testuje@wp.pl", "", ""},
        };
    }

    Helper helper = new Helper();

    @Severity(SeverityLevel.MINOR)
    @Test(dataProvider = "passToShort", description = "Checking basic account functions, i.e. changing password (for too short)")
    public void setShortPass(String oldPass, String newPass) {
        System.out.println("Rozpoczęto test sprawdzający funkcję zmiany hasła z wykorzystaniem zbyt krótkiego nowego hasła");

        WebDriver driver = getDriver();
        LogInScreen logInScreen = PageFactory.initElements(driver, LogInScreen.class);
        AccountScreen accountScreen = PageFactory.initElements(driver, AccountScreen.class);


        logInScreen.loginToApp("paula_testuje@wp.pl", "Tester2020OSW");
        logInScreen.dismissLogInPop();
        accountScreen.setAccount();
        accountScreen.accountSettings();
        accountScreen.passwordSettingsShort(oldPass, newPass);


        assertTrue(driver.findElement(By.id("pl.tablica:id/textinput_error"))
                .getText().equals("Ta wartość jest zbyt krótka. Powinna mieć 6 lub więcej znaków."));
        System.out.println("Test zaliczony - nie zmieniono hasła");

        helper.getScreenShot("Account OLX Android Test - changePassShortPass.png");

    }
    @Severity(SeverityLevel.MINOR)
    @Test(dataProvider = "wrongOldPass", description = "Checking basic account functions, i.e. changing password (wrong old password)")
    public void setWrongPass(String oldPass, String newPass) {
        System.out.println("Rozpoczęto test sprawdzający funkcję zmiany hasła z wykorzystaniem błędnego obecnego hasła");

        WebDriver driver = getDriver();
        LogInScreen logInScreen = PageFactory.initElements(driver, LogInScreen.class);
        AccountScreen accountScreen = PageFactory.initElements(driver, AccountScreen.class);

        logInScreen.loginToApp("paula_testuje@wp.pl", "Tester2020OSW");
        logInScreen.dismissLogInPop();
        accountScreen.setAccount();
        accountScreen.accountSettings();
        accountScreen.passwordSettingsWrongOld(oldPass, newPass);

        assertTrue(driver.findElement(By.id("pl.tablica:id/textinput_error"))
                .getText().equals("Hasło nieprawidłowe"));
        System.out.println("Test zaliczony - nie zmieniono hasła");

        helper.getScreenShot("Account OLX Android Test - changePassWrongOldPass.png");

    }

    @Severity(SeverityLevel.MINOR)
    @Test(dataProvider = "wrongEmail", description = "Checking basic account functions, i.e. changing e-mail addres for wrong one")
    public void setWrongEmail(String wrongEmail) {
        System.out.println("Rozpoczęto test sprawdzający funkcję zmiany adresu mailowego z wykorzystaniem adresu z błędem");

        WebDriver driver = getDriver();
        LogInScreen logInScreen = PageFactory.initElements(driver, LogInScreen.class);
        AccountScreen accountScreen = PageFactory.initElements(driver, AccountScreen.class);

        logInScreen.loginToApp("paula_testuje@wp.pl", "Tester2020OSW");
        logInScreen.dismissLogInPop();
        accountScreen.setAccount();
        accountScreen.accountSettings();
        accountScreen.emailSettWrongEmail(wrongEmail);

        assertTrue(driver.findElement(By.id("pl.tablica:id/textinput_error"))
                .getText().equals("Niepoprawny adres e-mail"));
        System.out.println("Test zaliczony - nie zmieniono adresu mailowego");

        helper.getScreenShot("Account OLX Android Test - changeEmailWrongEmail.png");

    }
    @Severity(SeverityLevel.MINOR)
    @Test(dataProvider = "usedEmail", description = "Checking basic account functions, i.e. changing e-mail addres for already taken one")
    public void setUsedEmail(String usedEmail) {
        System.out.println("Rozpoczęto test sprawdzający funkcję zmiany adresu mailowego z wykorzystaniem adresu już istniejącego");

        WebDriver driver = getDriver();
        LogInScreen logInScreen = PageFactory.initElements(driver, LogInScreen.class);
        AccountScreen accountScreen = PageFactory.initElements(driver, AccountScreen.class);

        logInScreen.loginToApp("paula_testuje@wp.pl", "Tester2020OSW");
        logInScreen.dismissLogInPop();
        accountScreen.setAccount();
        accountScreen.accountSettings();
        accountScreen.emailSettUsedEmail(usedEmail);

        assertTrue(driver.findElement(By.id("pl.tablica:id/textinput_error"))
                .getText().equals("Podany adres email już istnieje"));
        System.out.println("Test zaliczony - nie zmieniono adresu mailowego");

        helper.getScreenShot("Account OLX Android Test - changeEmailWrongEmail.png");

    }
    @Severity(SeverityLevel.MINOR)
    @Test(dataProvider = "editProfile", description = "Checking basic account functions, i.e. editing profile name")
    public void editProfile(String name) {
        System.out.println("Rozpoczęto test sprawdzający funkcję zmiany imienia użytkownika");

        WebDriver driver = getDriver();
        LogInScreen logInScreen = PageFactory.initElements(driver, LogInScreen.class);
        AccountScreen accountScreen = PageFactory.initElements(driver, AccountScreen.class);

        logInScreen.loginToApp("paula_testuje@wp.pl", "Tester2020OSW");
        logInScreen.dismissLogInPop();
        accountScreen.setAccount();
        accountScreen.accountSettings();
        accountScreen.editProfile(name);

        helper.getScreenShot("Account OLX Android Test - editProfile.png");
    }
    @Severity(SeverityLevel.MINOR)
    @Test(description = "Checking basic account functions, i.e. notifications settings")
    public void notificationsSett() {
        System.out.println("Rozpoczęto test sprawdzający funkcje Konta użytkownika");

        WebDriver driver = getDriver();
        LogInScreen logInScreen = PageFactory.initElements(driver, LogInScreen.class);
        AccountScreen accountScreen = PageFactory.initElements(driver, AccountScreen.class);

        logInScreen.loginToApp("paula_testuje@wp.pl", "Tester2020OSW");
        logInScreen.dismissLogInPop();
        accountScreen.setAccount();
        accountScreen.accountSettings();
        accountScreen.notificationsSettings();

     //   Assert.assertFalse(accountScreen.btn_undo.getText().equals("Ustawienia"), "Udało się zmienić ustawienia powiadomień.");
        helper.getScreenShot("Account OLX Android Test - notifications.png");
    }
    @Severity(SeverityLevel.MINOR)
    @Test(dataProvider = "invoiceSett", description = "Checking basic account functions, i.e. changing invoice data")
    public void setInvoiceDetails(String company, String street, String postcode, String city, String nip, boolean condition) {
        System.out.println("Rozpoczęto test sprawdzający funkcje Konta użytkownika");

        WebDriver driver = getDriver();
        LogInScreen logInScreen = PageFactory.initElements(driver, LogInScreen.class);
        AccountScreen accountScreen = PageFactory.initElements(driver, AccountScreen.class);

        logInScreen.loginToApp("paula_testuje@wp.pl", "Tester2020OSW");
        logInScreen.dismissLogInPop();
        accountScreen.setAccount();
        accountScreen.accountSettings();
        accountScreen.invoiceDetailsSettings(company, street, postcode, city, nip);

//        if (condition) {
//            assertTrue(driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/" +
//                    "android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view." +
//                    "View[4]/android.view.View/android.view.View/android.view.View"))
//                    .getText().equals("Twoje dane do faktury zostały zapisane"));
//            System.out.println("Test zaliczony - zmieniono dane do faktury");
//        } else {
//            Assert.fail("Assert failed");
//        }
        helper.getScreenShot("Account OLX Android Test - invoice.png");
    }

    @Severity(SeverityLevel.MINOR)
    @Test(description = "Checking basic account functions, i.e. adding cv")
    public void setCV() {
        System.out.println("Rozpoczęto test sprawdzający funkcje Konta użytkownika");

        WebDriver driver = getDriver();
        LogInScreen logInScreen = PageFactory.initElements(driver, LogInScreen.class);
        AccountScreen accountScreen = PageFactory.initElements(driver, AccountScreen.class);

        logInScreen.loginToApp("paula_testuje@wp.pl", "Tester2020OSW");
        logInScreen.dismissLogInPop();
        accountScreen.setAccount();
        accountScreen.accountSettings();
        accountScreen.addCVSettings();


     //   Assert.assertFalse(accountScreen.btn_chooseCVpdf.getText().equals("Twoje CV"), "Udało się dodać CV");
        helper.getScreenShot("Account OLX Android Test - CV.png");
    }
    @Severity(SeverityLevel.MINOR)
    @Test(description = "Checking basic account functions, i.e. changing mode")
    public void changeMode() {
        System.out.println("Rozpoczęto test sprawdzający funkcje Konta użytkownika");

        WebDriver driver = getDriver();
        LogInScreen logInScreen = PageFactory.initElements(driver, LogInScreen.class);
        AccountScreen accountScreen = PageFactory.initElements(driver, AccountScreen.class);

        logInScreen.loginToApp("paula_testuje@wp.pl", "Tester2020OSW");
        logInScreen.dismissLogInPop();
        accountScreen.setAccount();
        accountScreen.accountSettings();
        accountScreen.changeMode();

       //Assert.assertFalse(accountScreen.btn_undo.getText().equals("Ustawienia"), "Udało się zmienić tryb aplikacji.");
    }


    @AfterMethod
    public void tearDown2() {
        System.out.println("Test zakończono");
    }
}

