package com.selenium.java.tests.test.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.Helper;
import com.selenium.java.pages.android.AddAdWithPhotoScreen;
import com.selenium.java.pages.android.AddAdWithoutPhotoScreen;
import com.selenium.java.pages.android.LogInScreen;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.*;

public class AddAdAndroidTest extends BaseTest {

    @BeforeClass(description = "Starting appium server")
    public void startup() {
        startAppiumServer();
    }

    @BeforeMethod
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceId, String deviceName, ITestContext context) {

        instalApp = false;
        launchAndroid(platform, deviceId, deviceName, context);
    }

    @DataProvider
    Object[][] addWithPhoto(){
        return new Object[][]{
                {"testowanie_aplikacji@wp.pl", "Tester2020OSW", "Płyta CD Alice in Chains - Dirt", "Płyty CD", "Muzyka i Edukacja", "Muzyka", true, true, "25",
                        "Witam, \nmam na sprzedaż płytę Dirt zespołu Alice in Chains. Do ceny kompletu należy doliczyć koszty ewentualnej dostawy. \n" +
                        "                \nZainteresowane osoby zapraszam do kontaktu."},
                {"testowanie_aplikacji@wp.pl", "Tester2020OSW", "Monitor Dell P2219H", "Monitory", "Elektronika", "Komputery", true, false, "500",
                        "Witam, \nmam na sprzedaż Monitor Dell P2219H \n" +
                        "                \nZainteresowane osoby zapraszam do kontaktu."},
                {"testowanie_aplikacji@wp.pl", "Tester2020OSW", "Zestaw mebli ogrodowych", "Meble ogrodowe", "Dom i Ogród", "Ogród", false, true, "1000",
                        "Witam, \nmam na sprzedaż zestaw mebli ogrodowych zawierający dwa krzesła i stolik " +
                        "                \nZainteresowane osoby zapraszam do kontaktu." }

        };
    }

    @DataProvider
    Object[][] addWithoutPhoto(){
        return new Object[][] {
                {"testowanie_aplikacji@wp.pl", "Tester2020OSW", "Lustrzanka Nikon Coolpix P900", "Lustrzanki", "Elektronika", "Fotografia", true, true, "1700",
                        "Krótki opis przedmiotu", "576699671"},
                {"testowanie_aplikacji@wp.pl", "Tester2020OSW", "CREME Mia rower dziecięcy biegowy", "Rowery dziecięce", "Rowery", "Sport i Hobby", true, false, "200",
                        "Krótki opis przedmiotu.", "576699671"},
        };
    }

    Helper helper = new Helper();
    @Severity(SeverityLevel.MINOR)
    @Test (dataProvider = "addWithPhoto", description = "Checking basic functionality as adding advertisement with photo")
    public void testAddAdWithPhoto(String email, String password, String title, String catTarg, String cat1, String cat2,
                                   boolean privacy, boolean used,  String price, String description) {
        System.out.println("Rozpoczęto test dodawania ogłoszenia ze zdjęciem");

        WebDriver driver = getDriver();
        LogInScreen logInScreen = PageFactory.initElements(driver, LogInScreen.class);
        AddAdWithPhotoScreen addScreen = PageFactory.initElements(driver, AddAdWithPhotoScreen.class);

        System.out.println("Następuje logowanie się do aplikacji");
        logInScreen.loginToApp(email, password);
        System.out.println("Czekamy aż pop-up zniknie..");
        logInScreen.dismissLogInPop();
        addScreen.addAdvert();
        addScreen.addPhoto();
        addScreen.setTitle(title);
        addScreen.chooseCategory(catTarg, cat1, cat2);
        addScreen.setPrivacy(privacy);
        addScreen.setCondition(used);
        addScreen.setPrice(price);
        addScreen.negotiatingPrice();
        addScreen.addDescription(description);
        addScreen.setAdv(title);

//

        helper.getScreenShot("Advert OLX Test - with photo.png");

    }

    @Severity(SeverityLevel.MINOR)
    @Test (dataProvider = "addWithoutPhoto", description = "Checking basic functionality as adding advertisement with photo")
    public void testAddAdWithoutPhoto(String email, String password, String title, String catTarg, String cat1, String cat2,
                                      boolean privacy, boolean used,  String price, String description, String phoneNumber) {
        System.out.println("Rozpoczęto test dodawania ogłoszenia (bez zdjęcia)");

        WebDriver driver = getDriver();
        LogInScreen logInScreen = PageFactory.initElements(driver, LogInScreen.class);
        AddAdWithoutPhotoScreen advScreen = PageFactory.initElements(driver, AddAdWithoutPhotoScreen.class);

        System.out.println("Następuje logowanie się do aplikacji");
        logInScreen.loginToApp(email, password);
        System.out.println("Czekamy aż pop-up zniknie..");
        logInScreen.dismissLogInPop();
        advScreen.addAdvert();
        advScreen.setTitle(title);
        advScreen.chooseCategory(catTarg, cat1, cat2);
        advScreen.setPrivacy(privacy);
        advScreen.setCondition(used);
        advScreen.setPrice(price);
        advScreen.negotiatingPrice();
        advScreen.addDescription(description);
        advScreen.addPhoneNumber(phoneNumber);
        advScreen.addAdvert(title);

//        assertTrue(driver.findElement(By.id("pl.tablica:id/previewAdBtn"))
//                .getText().equals("Zobacz przed dodaniem"));
//        System.out.println("Test zaliczony - dodano nowe ogłoszenie");

        helper.getScreenShot("Advert OLX Test - with photo.png");
    }


    @AfterMethod
    public void tearDown2() {
        System.out.println("Test zakończono");
    }
}
