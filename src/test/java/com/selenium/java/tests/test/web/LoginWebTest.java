package com.selenium.java.tests.test.web;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.WebHelper;
import com.selenium.java.listeners.TestListener;
import com.selenium.java.pages.web.HomePage;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.*;
import static org.testng.Assert.assertTrue;

@Listeners({TestListener.class})
public class LoginWebTest extends BaseTest {

    @BeforeMethod(description = "Starting appium server")
    @Parameters(value = {"Platform", "Browser"})
    public void setUp(String platform, String browser, ITestContext context) {

        headless = false;// dotyczy tego czy testy otwierają się w przeglądarce czy też nie
        url = "https://www.olx.pl";

        launchWeb(platform, browser, context);
        System.out.println("Testy rozpoczęto");
    }



    //region DATAPROVIDERS
    @DataProvider
    public Object[][] wrongPassword() {
        return new Object[][]{
                {"paula_testuje@onet.pl", "Tester2020"},
                {"paula_testuje1@onet.pl", "ster2020OSW"},
                {"paula_testuje2@onet.pl", "tester2020OSW"},
                {"paula_testuje3@onet.pl", "TesterOSW"},
                {"paula_testuje4@onet.pl", "Tester"}
        };
    }

    @DataProvider
    public Object[][] wrongEmail() {
        return new Object[][]{
                {"paula_testuje@onet.", "Tester2020"},
                {"paula_testuje1@", "ster2020OSW"},
                {"@onet.pl", "tester2020OSW"},
                {"paula_testuje3@onet@.pl", "TesterOSW"},
                {"paula_testuje4@.pl", "Tester"}
        };
    }

    @DataProvider
    public Object[][] noEmail() {
        return new Object[][]{
                {"", "Tester2020OSW"},
                {"", "Tester2020OSW"},
        };
    }

    @DataProvider
    public Object[][] noPass() {
        return new Object[][]{
                {"paula_testuje@wp.pl", ""},
                {"testowanie_aplikacji@wp.pl", ""},
        };
    }


    @DataProvider
    public Object[][] properLogin() {
        return new Object[][]{
                {"paula_testuje@wp.pl", "Tester2020OSW"},
                {"testowanie_aplikacji@wp.pl", "Tester2020OSW"},
        };
    }

    //endregion DATAPROVIDERS


    //region TESTS

    WebHelper helper = new WebHelper();

    //region TEST = wrong password
    @Severity(SeverityLevel.MINOR)
    @Test(dataProvider = "wrongPassword", description = "Checking the basic functionalities of the main menu, i.e. logging in with the wrong password")
    public void wrongPasswordTest(String email, String pass) {

        System.out.println("Rozpoczęto test sprawdzający logowanie się z wykorzystaniem błędnego hasła");

        WebDriver driver = getDriver();
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);

        homePage.closeCookies();
        homePage.loginToApp(email, pass);

        helper.testScreenShoot("LoginWebTest");

        assertTrue(driver.findElement(By.id("se_emailError"))
                .getText().equals("nieprawidłowy login lub hasło"));
        System.out.println("Test zaliczony - nie zalogowano użytkownika");

     //   helper.getScreenShot("LogIn OLX Web Test - wrong password.png");

    }
    //endregion TEST = wrong password

    //region TEST = wrong email
    @Severity(SeverityLevel.MINOR)
    @Test(dataProvider = "wrongEmail", description = "Checking the basic functionalities of the main menu, i.e. logging in with the wrong e-mail address")
    public void wrongEmailTest(String email, String pass) {

        System.out.println("Rozpoczęto test sprawdzający logowanie się z wykorzystaniem błędnego adresu mailowego");

        WebDriver driver = getDriver();
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);


        homePage.closeCookies();
        homePage.loginToApp(email, pass);

        helper.testScreenShoot("LoginWebTest");

        assertTrue(driver.findElement(By.id("se_emailError"))
                .getText().equals("Niepoprawny format e-mail"));
        System.out.println("Test zaliczony - nie zalogowano użytkownika");

       // helper.getScreenShot("LogIn OLX Web Test - wrong email.png");
    }

    //endregion TEST = wrong email

    //region TEST = proper log in
    @Severity(SeverityLevel.MINOR)
    @Test(dataProvider = "properLogin", description = "Checking tthe basic functionalities of the main menu, i.e. logging " +
            "in with the correct data but without an account")
    public void properLoginTest(String email, String pass) {

        System.out.println("Rozpoczęto test sprawdzający logowanie się z wykorzystaniem poprawnych danych");

        WebDriver driver = getDriver();
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);


        homePage.closeCookies();
        homePage.loginToApp(email, pass);

        helper.testScreenShoot("LoginWebTest");

        //asercje
        assertTrue(driver.findElement(By.id("se_accountAnswers"))
                .getText().equals("Wiadomości"));
        System.out.println("Test zaliczony - użytkownik został zalogowany do aplikacji");

       // helper.getScreenShot("LogIn OLX Web Test - proper login.png");
    }

    //endregion TEST = proper log in

    //region TEST = no email
    @Severity(SeverityLevel.MINOR)
    @Test(dataProvider = "noEmail", description = "Checking the basic functionalities of the main menu, i.e. logging in without e-mail address")
    public void noEmailTest(String email, String pass) {

        System.out.println("Rozpoczęto test sprawdzający logowanie się bez wpisywania adresu mailowego");

        WebDriver driver = getDriver();
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);


        homePage.closeCookies();
        homePage.loginToApp(email, pass);

        helper.testScreenShoot("LoginWebTest");

        //asercje
        assertTrue(driver.findElement(By.id("se_emailError"))
                .getText().equals("To pole jest wymagane"));
        System.out.println("Test zaliczony - nie zalogowano użytkownika");

      //  helper.getScreenShot("LogIn OLX Web Test - no email.png");
    }

    //endregion TEST = no email

    //region TEST = no password
    @Severity(SeverityLevel.MINOR)
    @Test(dataProvider = "noPass", description = "Checking the basic functionalities of the main menu, i.e. logging in without password")
    public void noPassTest(String email, String pass) {

        System.out.println("Rozpoczęto test sprawdzający logowanie się bez wpisywania hasła");

        WebDriver driver = getDriver();
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);


        homePage.closeCookies();
        homePage.loginToApp(email, pass);

        helper.testScreenShoot("LoginWebTest");

        //asercje
        assertTrue(driver.findElement(By.id("se_emailError"))
                .getText().equals("To pole jest wymagane"));
        System.out.println("Test zaliczony - nie zalogowano użytkownika");

     //   helper.getScreenShot("LogIn OLX Web Test - no password.png");
    }

    //endregion TEST = no password

    //endregion TEST

    @AfterMethod
    public void tearDown() {

        WebDriver driver = getDriver();

        System.out.println("Test zakończono");
        driver.close();
    }

}
