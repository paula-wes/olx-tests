package com.selenium.java.tests.test.web;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.WebHelper;
import com.selenium.java.listeners.TestListener;
import com.selenium.java.pages.web.AccountPage;
import com.selenium.java.pages.web.HomePage;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;


import static org.testng.Assert.assertTrue;

@Listeners({TestListener.class})
public class AccountWebTest extends BaseTest {

    @BeforeMethod(description = "Starting appium server")
    @Parameters(value = {"Platform", "Browser"})
    public void setUp(String platform, String browser, ITestContext context) {

        headless = false;
        url = "https://www.olx.pl";

        launchWeb(platform, browser, context);
        System.out.println("Testy rozpoczęto");

        // (description = "Starting appium server")
    }

    //region DATAPROVIDERY
    @DataProvider
    public Object[][] email() {
        return new Object[][]{
                {"paula_testuje@wp.pl", "Tester2020OSW", "paula@"},
                {"paula_testuje@wp.pl", "Tester2020OSW", "paula@wp"},
                {"paula_testuje@wp.pl", "Tester2020OSW", "paula_testuje@@wp.pl"},
                {"paula_testuje@wp.pl", "Tester2020OSW", "testowanie_aplikacji@.pl"}
        };
    }

    @DataProvider
    public Object[][] password() {
        return new Object[][]{
                {"paula_testuje@wp.pl", "Tester2020OSW", "Tester2020OSW", "mama"},
                {"paula_testuje@wp.pl", "Tester2020OSW", "Tester2020OSW", "Tester2020OSW"}, // tu ma się pojawić błąd! / informacja, że hasło powinno się różnić
                {"paula_testuje@wp.pl", "Tester2020OSW", "Tester2020OSW", "123"}
        };
    }

    @DataProvider
    public Object[][] invoice() {
        return new Object[][]{
                {"paula_testuje@wp.pl", "Tester2020OSW", "", "Testerska 2a", "00-020", "Javka", "08025480", false},
                {"paula_testuje@wp.pl", "Tester2020OSW", "Testerka", "", "88-000", "Test", "08080565",false},
                {"paula_testuje@wp.pl", "Tester2020OSW", "Testujemy sp z o.o.", "Testerska 2a", "", "Javovo", "0808085656", false},
                {"paula_testuje@wp.pl", "Tester2020OSW", "Teścinki sp. z o.o. ", "Testerska 2a", "11-111", "", "080805656", false},
                {"paula_testuje@wp.pl", "Tester2020OSW", "Testy", "Testerska 2a", "22-222", "Test", "", true},
                {"paula_testuje@wp.pl", "Tester2020OSW", "Testerka", "Testerska 2a", "88-000", "Test", "05656805680", true}
        };
    }
    //endregion DATAPROVIDERY

    //region TESTS
    WebHelper helper = new WebHelper();

    //region TEST - account settings
    @Test
    public void accountSettTest() {
        System.out.println("Rozpoczęto test sprawdzający opcję zmiany danych kontaktowych");

        WebDriver driver = getDriver();
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        AccountPage accPage = PageFactory.initElements(driver, AccountPage.class);

        homePage.closeCookies();
        homePage.loginToApp("paula_testuje@wp.pl", "Tester2020OSW");
        accPage.goToMyAccount();
        accPage.goToSettings();
        accPage.changeContactData("Warszawa", "Mazowieckie");
        helper.testScreenShoot("AccountWebTest");


        assertTrue(driver.findElement(By.id("message_system"))
                .getText().equals("Zmiany zostały zapisane. Będą widoczne w ciągu 15 minut"));
        System.out.println("Test zaliczony - zmiany zostały zapisane");

      //  helper.getScreenShot("Account OLX Web Test - settings.png");
    }
    //endregion TEST - account settings

    //region TEST - attach CV
    @Severity(SeverityLevel.MINOR)
    @Test(description = "Checking account functionalities, i.e. attaching cv")
    public void attachCVTest() {
        System.out.println("Rozpoczęto test sprawdzający opcję zmiany danych kontaktowych");

        WebDriver driver = getDriver();
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        AccountPage accPage = PageFactory.initElements(driver, AccountPage.class);

        homePage.closeCookies();
        homePage.loginToApp("testowanie_aplikacji@wp.pl", "Tester2020OSW");
        accPage.goToMyAccount();
        accPage.goToSettings();
        accPage.attachCV("/Users/paula/Pictures/cv (1).jpg");
        helper.testScreenShoot("AccountWebTest");

        assertTrue(driver.findElement(By.className("olx-modal-box__title"))
                .getText().equals("Twoje CV zostało zapisane!"));
        System.out.println("Test zaliczony - CV zostało załączone");

        //helper.getScreenShot("Account OLX Web Test - attach CV.png");
    }
    //endregion TEST - attach CV

    //region TEST - change email
    @Severity(SeverityLevel.MINOR)
    @Test(dataProvider = "email", description = "Checking basic account functionalities, i.e. changing your email address")
    public void changeMailTest(String email, String pass, String mail) {
        System.out.println("Rozpoczęto test sprawdzający opcje ustawień - dołączanie cv, zmiana danych do faktury");

        WebDriver driver = getDriver();
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        AccountPage accPage = PageFactory.initElements(driver, AccountPage.class);
        homePage.closeCookies();
        homePage.loginToApp(email, pass);
        accPage.goToMyAccount();
        accPage.goToSettings();
        accPage.changeEmail(mail);

        assertTrue(driver.findElement(By.id("se_emailError"))
                .getText().equals("Niepoprawny format e-mail"));
        System.out.println("Test zaliczony - zmiany zostały zapisane");
        helper.testScreenShoot("AccountWebTest");

     //   helper.getScreenShot("Account OLX Web Test - change email.png");
    }

    //endregion TEST - change email

    //region TEST - change password
    @Severity(SeverityLevel.MINOR)
    @Test(dataProvider = "password", description = "Checking basic account functionalities, i.e. changing your password")
    public void changePassTest(String email, String pass, String oldPass, String newPass) {
        System.out.println("Rozpoczęto test sprawdzający opcje ustawień - dołączanie cv, zmiana danych do faktury");

        WebDriver driver = getDriver();
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        AccountPage accPage = PageFactory.initElements(driver, AccountPage.class);
        homePage.closeCookies();
        homePage.loginToApp(email, pass);
        accPage.goToMyAccount();
        accPage.goToSettings();
        accPage.changePassword(oldPass, newPass);
        helper.testScreenShoot("AccountWebTest");

        assertTrue(driver.findElement(By.id("se_emailError"))
                .getText().equals("Proszę wpisać co najmniej 8 znaków."));
        System.out.println("Test zaliczony - nie udało się zmienić hasła");

     //   helper.getScreenShot("Account OLX Web Test - change password.png");
    }
    //endregion TEST - change password

    //region TEST - invoice details
    @Severity(SeverityLevel.MINOR)
    @Test(dataProvider = "invoice", description = "Checking basic account functionalities, i.e. changing invoice data")
    public void invoiceSettTest(String email, String pass, String name, String street, String postcode, String city,
                                String nip, boolean condition) {
        System.out.println("Rozpoczęto test sprawdzający opcje ustawień");

        WebDriver driver = getDriver();
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        AccountPage accPage = PageFactory.initElements(driver, AccountPage.class);

        homePage.closeCookies();
        homePage.loginToApp(email, pass);
        accPage.goToMyAccount();
        accPage.goToSettings();
        accPage.invoiceDetails(name, street, postcode, city, nip);
        helper.testScreenShoot("AccountWebTest");

         if (condition) {
            assertTrue(driver.findElement(By.className("confirm-polaroid__message"))
                    .getText().equals("Twoje dane do faktury zostały zapisane"));
            System.out.println("Test zaliczony - wprowadzono dane do faktury");
        } else {
            Assert.fail("Assert failed");
        }

//        if (condition) {
//            assertTrue(driver.findElement(By.id("se_emailError"))
//                    .getText().equals("To pole jest wymagane"));
//            System.out.println("Test zaliczony - nie wprowadzono danych do faktury");
//        } else if (condition) {
//            assertTrue(driver.findElement(By.className("confirm-polaroid__message"))
//                    .getText().equals("Twoje dane do faktury zostały zapisane"));
//            System.out.println("Test zaliczony - wprowadzono dane do faktury");
//        } else {
//            Assert.fail("Assert failed");
//        }

     //   helper.getScreenShot("Account OLX Web Test - invoice details.png");

    }
    //endregion TEST - invoice details

    //region TEST - log out
    @Severity(SeverityLevel.MINOR)
    @Test(description = "Checking basic account functionalities, i.e. logging out")
    public void logOutTest() {
        System.out.println("Rozpoczęto test sprawdzający opcję zmiany danych kontaktowych");

        WebDriver driver = getDriver();
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        AccountPage accPage = PageFactory.initElements(driver, AccountPage.class);

        homePage.closeCookies();
        homePage.loginToApp("paula_testuje@wp.pl", "Tester2020OSW");
        accPage.logOut();
        helper.testScreenShoot("AccountWebTest");

//        try {
//            driver.findElement(By.id("se_accountAnswers"));
//        }catch (NoSuchElementException e) {
//            Assert.fail("Nie znaleziono elementu");
//        }
//        assertTrue(driver.findElement(By.id("se_accountAnswers")).isDisplayed());

        assertTrue(driver.findElement(By.xpath("//*[@id=\"searchmain-container\"]/div[1]/div/header/h3"))
                .getText().equals("Kategorie główne"));
        System.out.println("Test zaliczony -  wylogowano użytkownika");
    //    helper.getScreenShot("Account OLX Web Test - log out.png");

    }




    //endregion TEST - log out

    //endregion TESTS

    @AfterMethod
    public void tearDown() {

        WebDriver driver = getDriver();
        System.out.println("Test zakończono");
         driver.close();

    }

}
