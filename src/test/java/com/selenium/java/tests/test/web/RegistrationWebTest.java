package com.selenium.java.tests.test.web;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.WebHelper;
import com.selenium.java.listeners.TestListener;
import com.selenium.java.pages.web.HomePage;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.*;

import static org.testng.Assert.assertTrue;


@Listeners({TestListener.class})
public class RegistrationWebTest extends BaseTest {

    @BeforeMethod(description = "Starting appium server")
    @Parameters(value = {"Platform", "Browser"})
    public void setUp(String platform, String browser, ITestContext context) {

        headless = false;
        url = "https://www.olx.pl";

        launchWeb(platform, browser, context);
        System.out.println("Testy rozpoczęto");
    }

    //region DATAPROVIDERS

    @DataProvider
    public Object[][] noPass() {
        return new Object[][]{
                {"paula_testuje@onet.pl", ""},
                {"paula_testuje1@onet.pl", ""},
                {"paula_testuje2@onet.pl", ""},
        };
    }

    @DataProvider
    public Object[][] noEmail() {
        return new Object[][]{
                {"", "Tester2020OSW"},
                {"", "Tester2020"},
                {"", "TesterOSW2"},

        };
    }

    @DataProvider
    public Object[][] weakPass() {
        return new Object[][]{
                {"paula_testuje@onet.pl", "aaaaaaaaa"},
                {"paula_testuje1@onet.pl", "12345678"},
                {"paula_testuje2@onet.pl", "MAMAMAMA"},
        };
    }

    @DataProvider
    public Object[][] tooShortPass() {
        return new Object[][]{
                {"paula_testuje@onet.pl", "ala"},
                {"paula_testuje1@onet.pl", "123"},
                {"paula_testuje2@onet.pl", "mama"},
        };
    }

    @DataProvider
    public Object[][] wrongEmail() {
        return new Object[][]{
                {"paula", "Tester2020OSW"},
                {"paula_", "Tester2020OSW"},
                {"paula_testuje", "Tester2020OSW"},
                {"paula_testuje@", "Tester2020OSW"},
                {"paula_testuje@wp", "Tester2020OSW"}
        };
    }

    //endregion DATAPROVIDERS


    //region TESTS
    WebHelper helper = new WebHelper();

    @Severity(SeverityLevel.MINOR)
    @Test(dataProvider = "noPass", description = "Checking the basic functionalities of the main menu, i.e. registering without a password")
    public void noPassRegisterTest(String email, String pass) {

        System.out.println("Rozpoczęto test sprawdzający rejestrowanie użytkownika bez wpisywania hasła");

        WebDriver driver = getDriver();
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);

        homePage.closeCookies();
        homePage.registerToApp(email, pass);

        helper.testScreenShoot("RegistrationWebTest");

        assertTrue(driver.findElement(By.id("se_emailError"))
                .getText().equals("To pole jest wymagane"));
        System.out.println("Test zaliczony - nie zarejestrowano użytkownika");

       // helper.getScreenShot("Registration OLX Web Test - no password.png");
    }

    @Severity(SeverityLevel.MINOR)
    @Test(dataProvider = "noEmail", description = "Checking the basic functionalities of the main menu, i.e. registering without an e-mail address")
    public void noEmailRegisterTest(String email, String pass) {

        System.out.println("Rozpoczęto test sprawdzający rejestrowanie użytkownika bez wpisywania hasła");

        WebDriver driver = getDriver();
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);

        homePage.closeCookies();
        homePage.registerToApp(email, pass);

        helper.testScreenShoot("RegistrationWebTest");

        assertTrue(driver.findElement(By.id("se_emailError"))
                .getText().equals("To pole jest wymagane"));
        System.out.println("Test zaliczony - nie zarejestrowano użytkownika");

       // helper.getScreenShot("Registration OLX Web Test - no email.png");
    }

    @Severity(SeverityLevel.MINOR)
    @Test(dataProvider = "weakPass", description = "Checking the basic functionalities of the main menu, i.e. registering with a weak password")
    public void weakPassRegisterTest(String email, String pass) {

        System.out.println("Rozpoczęto test sprawdzający rejestrowanie użytkownika bez wpisywania hasła");

        WebDriver driver = getDriver();
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);

        homePage.closeCookies();
        homePage.registerToApp(email, pass);

        helper.testScreenShoot("RegistrationWebTest");

        assertTrue(driver.findElement(By.id("se_emailError"))
                .getText().equals("Hasło musi zawierać przynajmniej jedną cyfrę oraz jedną wielką i jedną małą literę"));
        System.out.println("Test zaliczony - nie zarejestrowano użytkownika");

     //   helper.getScreenShot("Registration OLX Web Test - weak password.png");
    }

    @Severity(SeverityLevel.MINOR)
    @Test(dataProvider = "tooShortPass", description = "Checking the basic functionalities of the main menu, i.e. registering with too short password")
    public void shortPassRegisterTest(String email, String pass) {

        System.out.println("Rozpoczęto test sprawdzający rejestrowanie użytkownika bez wpisywania hasła");

        WebDriver driver = getDriver();
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);

        homePage.closeCookies();
        homePage.registerToApp(email, pass);

        helper.testScreenShoot("RegistrationWebTest");

        assertTrue(driver.findElement(By.id("se_emailError"))
                .getText().equals("Proszę wpisać co najmniej 8 znaków."));
        System.out.println("Test zaliczony - nie zarejestrowano użytkownika");

      //  helper.getScreenShot("Registration OLX Web Test - weak password.png");
    }

    @Severity(SeverityLevel.MINOR)
    @Test(dataProvider = "wrongEmail", description = "Checking the basic functionalities of the main menu, i.e. registering with wrong e-mail address")
    public void wrongEmailRegisterTest(String email, String pass) {

        System.out.println("Rozpoczęto test sprawdzający rejestrowanie użytkownika bez wpisywania hasła");

        WebDriver driver = getDriver();
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);

        homePage.closeCookies();
        homePage.registerToApp(email, pass);

        helper.testScreenShoot("RegistrationWebTest");

        assertTrue(driver.findElement(By.id("se_emailError"))
                .getText().equals("Niepoprawny format e-mail"));
        System.out.println("Test zaliczony - nie zarejestrowano użytkownika");

     //   helper.getScreenShot("Registration OLX Web Test - wrong email.png");
    }


    @AfterMethod
    public void tearDown() {

        WebDriver driver = getDriver();

        System.out.println("Test zakończono");
        driver.close();
    }
}
