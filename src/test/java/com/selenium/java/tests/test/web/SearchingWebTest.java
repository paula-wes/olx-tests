package com.selenium.java.tests.test.web;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.WebHelper;
import com.selenium.java.listeners.TestListener;
import com.selenium.java.pages.web.HomePage;
import com.selenium.java.pages.web.SearchingPage;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.*;

import static org.testng.Assert.assertTrue;


@Listeners({TestListener.class})
public class SearchingWebTest extends BaseTest {

    @BeforeMethod(description = "Starting appium server")
    @Parameters(value = {"Platform", "Browser"})
    public void setUp(String platform, String browser, ITestContext context) {

        headless = false;
        url = "https://www.olx.pl";

        launchWeb(platform, browser, context);
        System.out.println("Testy rozpoczęto");
    }

    //region DATAPROVIDERS

    @DataProvider
    public Object[][] search() {
        return new Object[][]{
            //    {"Zimbardo", "Książki", true, true, "Warszawa", "Mazowieckie", "10", "120", true, "latest", true, "all"},
                {"Walizka", "Turystyka", false, false, "Gdańsk ", "Pomorskie", "100", "355", false, "cheapest", false, "company"},
           //     {"Tablet Lenovo", "Tablety", true, true, "Poznań ", "Wielkopolskie", "200", "500", true, "expensive", true, "private"},
        };
    }

    //endregion DATA PROVIDERS


    //region TESTS
    WebHelper helper = new WebHelper();
    @Severity(SeverityLevel.MINOR)
    @Test(dataProvider = "search", description = "Checking basic functionalities, i.e. searching for the selected item")
    public void searchingTest(String item, String category1, boolean searchInDesc, boolean photo,
                              String city, String voivodeship, String priceFrom, String priceTo, boolean condition, String sortBy, boolean view,
                              String priv) {

        System.out.println("Rozpoczęto test dotyczący wyszukiwania przemdiotu");

        WebDriver driver = getDriver();
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        SearchingPage searchPage = PageFactory.initElements(driver, SearchingPage.class);

        homePage.closeCookies();
        searchPage.searchItem(item, category1);
        searchPage.searchInDesc(searchInDesc);
        searchPage.searchWithPhoto(photo);
        searchPage.location(city, voivodeship);
        searchPage.setPrice(priceFrom, priceTo);
        searchPage.setCondition(condition);
        searchPage.sortBy(sortBy);
        searchPage.viewSelector(view);
        searchPage.setPrivacy(priv);
        searchPage.followSearches();
        helper.testScreenShoot("SearchingWebTest");

        assertTrue(driver.findElement(By.xpath("//*[@id=\"saveFavDiv\"]/div[2]/p/a/span"))
                .getText().equals("Nie, dziękuję"));
        System.out.println("Test zaliczony - wyszukano przedmiot i dodano wyszukiwanie do ulubionych");

      //  helper.getScreenShot("Searching OLX Web Test.png");
    }

    //endregion TESTS

    @AfterMethod
    public void tearDown() {

        WebDriver driver = getDriver();

        System.out.println("Test zakończono");
        driver.close();
    }
}
