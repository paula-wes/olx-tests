package com.selenium.java.tests.test.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.Helper;
import com.selenium.java.pages.android.CategoryScreen;
import com.selenium.java.pages.android.LogInScreen;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;

public class CategoryScreenAndroidTest extends BaseTest {
    @BeforeClass
    public void startup() {

        startAppiumServer();
    }

    @BeforeMethod(description = "Starting appium server")
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceId, String deviceName, ITestContext context) {

        instalApp = false;
        launchAndroid(platform, deviceId, deviceName, context);
    }

    Helper helper = new Helper();
    @Severity(SeverityLevel.MINOR)
    @Test(description = "Checking basic functionality of searching the item")
    public void testCategories() {

        System.out.println("Rozpoczeto test sprawdzający kategorie przedmiotów");

        WebDriver driver = getDriver();
        LogInScreen logInScreen = PageFactory.initElements(driver, LogInScreen.class);
        CategoryScreen catScreen = PageFactory.initElements(driver, CategoryScreen.class);

        System.out.println("Następuje logowanie się do aplikacji OLX");
        logInScreen.loginToApp("paula_testuje@wp.pl", "Tester2020OSW");
        System.out.println("Czekamy az zniknie pop-up");
        logInScreen.dismissLogInPop();;
        catScreen.setCategories();
        catScreen.filterLocation();
        catScreen.setPrice("0", "100");
        catScreen.setConditions();
        catScreen.addAdv();

        try {
            driver.findElement(By.id("pl.tablica:id/searchInput"));
        } catch (NoSuchElementException e) {
            Assert.fail("Nie udało się znaleźć przedmiotu "); // przykład działania asercji
        }
        Assert.assertTrue(driver.findElement(By.id("pl.tablica:id/searchInput")).isDisplayed());

        System.out.println("Znaleziono wyszukiwany przedmiot");
        helper.getScreenShot("Category OLX Android Test.png");

    }
    @AfterMethod
    public void tearDown2() {
        System.out.println("Test zakończono");
    }
}
