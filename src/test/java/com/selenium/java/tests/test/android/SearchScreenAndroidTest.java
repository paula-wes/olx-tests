package com.selenium.java.tests.test.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.Helper;
import com.selenium.java.pages.android.LogInScreen;
import com.selenium.java.pages.android.SearchScreen;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;

public class SearchScreenAndroidTest extends BaseTest {
    @BeforeClass(description = "Starting appium server")
    public void startup() {
        startAppiumServer();
    }

    @BeforeMethod
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceId, String deviceName, ITestContext context) {

        instalApp = false;
        launchAndroid(platform, deviceId, deviceName, context);
    }

    @DataProvider
    public Object[][] searchCar() {
        return new Object[][]{
                {"Mercedes", "Bydgoszcz", "200000", "500000", "2017", "2020"},
                {"Volkswagen", "Gdańsk", "10000", "20000", "2000", "2018"},
                {"Toyota", "Warszawa", "5000", "15000", "2015", "2017"},
        };
    }


    Helper helper = new Helper();
    @Severity(SeverityLevel.MINOR)
    @Test (dataProvider = "searchCar", description = "Checking basic functionality of searching item")
    public void testSearchingCar(String car, String city, String priceFrom, String priceTo,String yearProdFrom,
                                 String yearProdTo) {
        System.out.println("Rozpoczęto test sprawdzający funkcję search");

        WebDriver driver = getDriver();
        LogInScreen logInScreen = PageFactory.initElements(driver, LogInScreen.class);
        SearchScreen searchScreen = PageFactory.initElements(driver, SearchScreen.class);

        System.out.println("Następuje logowanie się do aplikacji");
        logInScreen.loginToApp("paula_testuje@wp.pl", "Tester2020OSW");
        System.out.println("Czekamy na zniknięcie pop-upu");
        logInScreen.dismissLogInPop();
        System.out.println("Korzystanie z wyszukiwarki na OLX");
        searchScreen.searchItems(car);
        System.out.println("Rozpoczęto filtrowanie");
        searchScreen.filterItems(city);
        System.out.println("Wpisano zakres ceny");
        searchScreen.setPrice(priceFrom, priceTo);
        System.out.println("Wpisano zakresu roku produkcji");
        searchScreen.setYearOfProd(yearProdFrom, yearProdTo);
        System.out.println("Wybieranie większej liczby parametrów");
        searchScreen.setFuel();
        searchScreen.bodyType();
        searchScreen.moreFunction();
        System.out.println("Pokazanie rezultatu wyszukiwań");
        searchScreen.showResults();


        try {
            driver.findElement(By.id("pl.tablica:id/searchInput"));
        } catch (NoSuchElementException e) {
            Assert.fail("Element nie został znaleziony. "); // przykład działania asercji
        }
        Assert.assertTrue(driver.findElement(By.id("pl.tablica:id/searchInput")).isDisplayed());
        System.out.println("Wyszukano przedmiot: " + car);
        helper.getScreenShot("Searching OLX Android Test.png");

    }
    @AfterMethod
    public void tearDown(){
        System.out.println("Zakończono test sprawdzający funkcję search");
    }
}
