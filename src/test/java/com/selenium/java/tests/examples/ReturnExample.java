package com.selenium.java.tests.examples;

import java.lang.reflect.Array;

public class ReturnExample {

    private enum Names {
        ALA,
        OLA,
        MAREK,
        MICHAL

    }


    public static void main(String[] args) {

        Names names = Names.MAREK;

        System.out.println(name(names) + " ma " + age(names) + " lat.");

        System.out.println(Array.get(twoInts(), 0));
        System.out.println(Array.get(twoInts(), 1));
        System.out.println("\n");
        System.out.println(Array.get(twoInts(), 2));
        System.out.println(Array.get(twoInts(), 3));

    }

    public static String name(Names names) {

        String firstName;

        switch (names) {

            case ALA:
                firstName = "Ala";
                break;

            case OLA:
                firstName = "Ola";
                break;

            case MAREK:
                firstName = "Marek";
                break;

            case MICHAL:
                firstName = "Michał";
                break;

            default:
                firstName = "Unknown";
        }

        return firstName;
    }


    public static int age(Names names) {
        int age;

        switch (names) {
            case ALA:
                age = 5;
                break;

            case OLA:
                age = 54;
                break;

            case MAREK:
                age = 31;
                break;

            case MICHAL:
                age = 25;
                break;

            default:
                age = 0;
        }
        return age;
    }

    public static int[] twoInts() {
        int a = 2;
        int b = 3;
        int c = 4;
        int d = 5;

        return new int[] {a, b, c, d};
    }
}