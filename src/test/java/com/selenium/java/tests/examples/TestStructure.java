package com.selenium.java.tests.examples;

import org.testng.annotations.*;

public class TestStructure {

    @BeforeClass
    public void startup()
    {
        System.out.println("BeforeClass działa");
    }

    @BeforeMethod
    public void setUp()
    {
        System.out.println("BeforeMethod działa");
    }

    @Test
    public void test1()
    {
        System.out.println("Test1 działa");
    }

    @Test
    public void test2()
    {
        System.out.println("Test2 działa");
    }

    @Test
    public void test3()
    {
        System.out.println("Test3 działa");
    }

    @Test
    public void test4()
    {
        System.out.println("Test4 działa");
    }

    @AfterMethod
    public void tearDown()
    {
        System.out.println("AfterMethod działa");
    }

    @AfterClass
    public void stop()
    {
        System.out.println("AfterClass działa");
    }






}
