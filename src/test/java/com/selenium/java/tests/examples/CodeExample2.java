package com.selenium.java.tests.examples;

public class CodeExample2 {

    public static void main(String[] args) {

        Person paula = new Person();

        paula.name = "Paula";
        paula.age = 25;
        paula.isAlive = true;

        Person antoni = new Person();

        antoni.name = "Antoni";
        antoni.age = 100;
        antoni.isAlive = false;

        paula.przedstawSie();
        antoni.przedstawSie();




        Person marek = new Person();

        marek.przedstawSie("Marek",18, true);

        Person andrzej = new Person();

        andrzej.przedstawSie("Andrzej", 66, false);

    }
}


